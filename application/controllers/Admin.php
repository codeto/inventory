<?php
/**
 * Created by Ha Anh Son
 * User: heyso
 * Date: 4/7/2016
 * Time: 9:45 AM
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $this->load->helper(array('form'));
        $this->load->view('admin_login');
    }

    function logout()
    {
        $this->session->unset_userdata('_admin_logged_in');
        session_destroy();
        redirect('/admin/index', 'refresh');
    }

}

?>