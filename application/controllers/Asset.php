<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asset extends CI_Controller
{

    // Load libraries in Constructor.
    function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        $this->load->helper('breadcrumb_helper');
        $this->load->model('Asset_Model');
        $this->load->model('Asset_Type_Model');
        $this->load->model('Supplier_Model');
        $this->load->model('Asset_Inventory_Model');
        $this->load->model('Asset_Category_Model');
        $this->load->model('Employee_Asset_Model');
        $this->load->model('Department_Model');
        $this->load->model('Employee_Model');
        $this->load->model('Staff_Model');
        $this->load->model('Common_Model');
        $this->load->library('session');
        $this->load->library('pagination');
    }


    public function quickAssigned($id)
    {
            $data['asset_item'] = $this->Asset_Model->get_asset($id);
            $data['all_department'] = $this->Department_Model->get_all_department();
//            $data['all_staff'] = $this->Staff_Model->get_all_staff();
            $data['all_staff'] = $this->Employee_Model->get_all_employee();
            echo json_encode($data);
            exit();
    }

    public function quickUnassigned($id)
    {
            $data['asset_item'] = $this->Asset_Model->get_asset($id);
            $data['category'] = $this->Asset_Category_Model->get_asset_category($data['asset_item']->a_category);
            $data['employee_asset'] =  $this->Employee_Asset_Model->get_employee_asset_by_asset_id($id);
            if($data['employee_asset']) {
                $data['staff'] =  $this->Employee_Model->get_employee($data['employee_asset']->ea_employee_id);
                $data['department'] = $this->Department_Model->get_department_name($data['staff']->em_department_id);
                echo json_encode($data);
                exit();
            }
        else {
            echo json_encode(array("status" => false));exit();
        }

    }

    public function ajax_add()
    {
        $this->Employee_Asset_Model->quick_set_employee_asset();
    }

    public function ajax_unassigned()
    {
        $asset_id = isset($_POST['asset_id']) ? trim($_POST['asset_id']) : '';
        $employee_asset_data = $this->Employee_Asset_Model->get_employee_asset_by_asset_id($asset_id);
        if($employee_asset_data) {
            $result = $this->Employee_Asset_Model->unassigned_asset($employee_asset_data->ea_seq, $asset_id);
            if($result) {
                echo json_encode(array("status" => true));exit();
            }
        }
        echo json_encode(array("status" => false));exit();
    }

    public function assigned($page_num = 1, $sortfield = 'ea_seq', $order = 'desc')
    {
        if ($this->session->userdata('logged_in')) {
            $data['title'] = 'List Asset Assigned';
            $this->load->helper('form');
            $data['all_search_field'] = array(
                '' => '--Option--',
                'ea_asset_id' => 'Asset ID',
                'as_asset_category_id' => 'Type',
                'as_supplier_id' => 'Supplier'
            );
            $config = $this->Common_Model->getPaging(5);
            $config["base_url"] = base_url() . "/index.php/asset/assigned";
            $type_search = ($this->input->post("type_search"))? $this->input->post("type_search") : null;
            $keyword = ($this->input->post("search_name"))? $this->input->post("search_name") : null;
            if(!empty($type_search) && !empty($keyword)) {
                $config["base_url"] = base_url() . "/index.php/asset/assigned/$type_search/$keyword";
            }
            $config["total_rows"] = $this->Employee_Asset_Model->record_count($type_search, $keyword);

            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $this->pagination->initialize($config);

            if (empty($page)) $page = 1;
            $offset = ($page - 1) * $config['per_page'];
            $data['page'] = (int)$this->uri->segment(3);
            $order_seg = $this->uri->segment(4, "desc"); // if the 5th segment not present,it will return asc. default value.
            $data["order"] = ($order_seg == "asc") ? 'asc' : 'desc';

            $data["results"] = $this->Employee_Asset_Model->fetch_employee_asset($config["per_page"], $offset, $sortfield, $order, null, $type_search, $keyword);
            $data["links"] = $this->pagination->create_links();
            $this->load->view('templates/header', $data);
            $this->load->view('asset/assigned');
            $this->load->view('templates/footer');
        } else {
            redirect('login', 'refresh');
        }
    }

    public function unassigned($page_num = 1, $sortfield = 'a_seq', $order = 'desc')
    {
        if ($this->session->userdata('logged_in')) {
            $data['title'] = 'List Asset Un-Assigned';
            $data['all_search_field'] = array(
                '' => '--Option--',
                'a_seq' => 'Asset ID',
                'a_category' => 'Type',
                'a_supplier_id' => 'Supplier'
            );
            $this->load->helper('form');
            $config = $this->Common_Model->getPaging(5);
            $config["base_url"] = base_url() . "/index.php/asset/unassigned";
            $type_search = ($this->input->post("type_search"))? $this->input->post("type_search") : null;
            $keyword = ($this->input->post("search_name"))? $this->input->post("search_name") : null;
            if(!empty($type_search) && !empty($keyword)) {
                $config["base_url"] = base_url() . "/index.php/asset/unassigned/$type_search/$keyword";
            }
            $config["total_rows"] = $this->Asset_Model->record_count_unassigned($type_search, $keyword);

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

            if (empty($page)) $page = 1;
            $offset = ($page - 1) * $config['per_page'];
            $data['page'] = (int)$this->uri->segment(3);
            $order_seg = $this->uri->segment(4, "asc"); // if the 5th segment not present,it will return asc. default value.
            $data["order"] = ($order_seg == "asc") ? 'desc' : 'asc';

            $data["results"] = $this->Employee_Asset_Model->fetch_unassigned($config["per_page"], $offset, $sortfield, $order, $type_search, $keyword);
//            var_dump($data["results"] );die;
            $data["links"] = $this->pagination->create_links();
            $this->load->view('templates/header', $data);
            $this->load->view('asset/unassigned');
            $this->load->view('templates/footer');
        } else {
            redirect('login', 'refresh');
        }
    }

    public function member($page_num = 1, $sortfield = 'a_seq', $order = 'desc')
    {
        if ($this->session->userdata('user_logged_in')) {
            $data['title'] = 'My Asset Assigned';
            $session_data = $this->session->userdata('user_logged_in');
            $member_id = $session_data['id'];

            $config = $this->Common_Model->getPaging();
            $config["base_url"] = base_url() . "/index.php/asset/employee";
            $config["total_rows"] = $this->Employee_Asset_Model->record_count_by_employee($member_id);

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

            if (empty($page)) $page = 1;
            $offset = ($page - 1) * $config['per_page'];
            $data['page'] = (int)$this->uri->segment(3);
            $order_seg = $this->uri->segment(5, "asc"); // if the 5th segment not present,it will return asc. default value.
            $data["order"] = ($order_seg == "asc") ? 'desc' : 'asc';

            $data["results"] = $this->Employee_Asset_Model->fetch_employee_asset($config["per_page"], $offset, $sortfield, $order, $member_id);
            $data["links"] = $this->pagination->create_links();
            $this->load->view('templates/header', $data);
            $this->load->view('asset/member');
            $this->load->view('templates/footer');
        } else {
            redirect('login', 'refresh');
        }
    }


    public function index($page_num = 1, $sortfield = 'a_seq', $order = 'desc')
    {
        if ($this->session->userdata('logged_in')) {
            $this->load->helper('form');
            $data['title'] = 'List Asset';
            $data['all_search_field'] = array(
                '' => '--Option--',
                'a_name' => 'Name',
                'a_category' => 'Type',
                'a_supplier_id' => 'Supplier'
            );
            $config = $this->Common_Model->getPaging(5);
            $config["base_url"] = base_url() . "/index.php/asset/index";
            $type_search = ($this->input->post("type_search"))? $this->input->post("type_search") : null;
            $keyword = ($this->input->post("search_name"))? $this->input->post("search_name") : null;
            if(!empty($type_search) && !empty($keyword)) {
                $config["base_url"] = base_url() . "/index.php/asset/index/$type_search/$keyword";
            }
            $config["total_rows"] = $this->Asset_Model->record_count($type_search, $keyword);

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

            if (empty($page)) $page = 1;
            $offset = ($page - 1) * $config['per_page'];
            $data['page'] = (int)$this->uri->segment(3);
            $order_seg = $this->uri->segment(5, "asc"); // if the 5th segment not present,it will return asc. default value.
            $data["order"] = ($order_seg == "asc") ? 'desc' : 'asc';

            $data["results"] = $this->Asset_Model->fetch_asset($config["per_page"], $offset, $sortfield, $order, $type_search, $keyword);
            $data["links"] = $this->pagination->create_links();
            $this->load->view('templates/header', $data);
            $this->load->view('asset/list');
            $this->load->view('templates/footer');
        } else {
            redirect('login', 'refresh');
        }
    }

    public function deleteMultiple() {
        $dat = $this->input->post('asset_id');
        for ($i = 0; $i < sizeof($dat); $i++) {
            $this->Asset_Model->delete_asset($dat[$i]);
        }
        redirect('asset/index', 'refresh');
    }

    public function deleteMultipleAssigned() {
        $dat = $this->input->post('asset_id');
        for ($i = 0; $i < sizeof($dat); $i++) {
            $this->Employee_Asset_Model->delete_employee_asset($dat[$i]);
        }
        redirect('asset/assigned', 'refresh');
    }

    public function deleteMultipleUnassigned() {
        $dat = $this->input->post('asset_id');
        for ($i = 0; $i < sizeof($dat); $i++) {
            $this->Asset_Model->delete_asset($dat[$i]);
        }
        redirect('asset/unassigned', 'refresh');
    }

    public function type($page_num = 1, $sortfield = 'at_seq', $order = 'desc')
    {
        if ($this->session->userdata('logged_in')) {
            $this->load->helper('form');
            $data['title'] = 'List of Categories';
            $config = $this->Common_Model->getPaging(5);
            $config["base_url"] = base_url() . "/index.php/asset/type";
            $total_row = $this->Asset_Type_Model->record_count();
            $config["total_rows"] = $total_row;

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

            if (empty($page)) $page = 1;
            $offset = ($page - 1) * $config['per_page'];
            $data['page'] = (int)$this->uri->segment(3);
            $order_seg = $this->uri->segment(5, "asc"); // if the 5th segment not present,it will return asc. default value.
            $data["order"] = ($order_seg == "asc") ? 'desc' : 'asc';

            $data["results"] = $this->Asset_Type_Model->fetch_asset_types($config["per_page"], $offset, $sortfield, $order);
            $data["links"] = $this->pagination->create_links();
            $this->load->view('templates/header', $data);
            $this->load->view('asset/list_type');
            $this->load->view('templates/footer');
        } else {
            redirect('login', 'refresh');
        }
    }

    function check_stock($ea_asset_id)
    {
        $ea_asset_id = $this->input->post('ea_asset_id');
        $this->load->model('Asset_Inventory_Model', 'asset_inventory');

        $result = $this->Asset_Inventory_Model->get_asset_inventory($ea_asset_id);
        if ($result->ai_number_in_stock > 0) {
            return true;
        } else {
            $this->form_validation->set_message('check_stock', 'Assets is out of ​stock');
            return false;
        }
    }

    function check_assigned($ea_employee_id)
    {
        $ea_asset_id = $this->input->post('ea_asset_id');
        $result = $this->Employee_Asset_Model->check_assigned($ea_employee_id, $ea_asset_id);
        if (!$result) {
            return true;
        } else {
            $this->form_validation->set_message('check_assigned', 'Assets is assigned for this user');
            return false;
        }
    }

    public function create_assigned()
    {
        if ($this->session->userdata('logged_in')) {
            $data['title'] = 'Create Assign';
            $this->load->helper('form');
            $this->load->library('form_validation');
            $this->form_validation->set_rules('ea_employee_id', 'Employee', 'required|callback_check_assigned');
            $this->form_validation->set_rules('ea_asset_id', 'Asset', 'required|callback_check_stock');
            $this->form_validation->set_rules('ea_date_out', 'Date Out', 'required');

            $data['all_staff'] = $this->Employee_Model->get_select_employee();
            $data['all_asset'] = $this->Asset_Model->get_select_asset();

            if ($this->form_validation->run() === FALSE) {
                $this->load->view('templates/header', $data);
                $this->load->view('asset/create_assigned');
                $this->load->view('templates/footer');
            } else {
                $this->Employee_Asset_Model->set_employee_asset();
                $this->session->set_flashdata('employee_asset_add', 'Add Assign Successful');
                redirect('/asset/assigned', 'refresh');
            }
        } else {
            redirect('login', 'refresh');
        }

    }

    public function create()
    {
        if ($this->session->userdata('logged_in')) {
            $data['title'] = 'Create a Asset';
            $this->load->helper('form');
            $this->load->library('form_validation');
            $this->form_validation->set_rules('a_name', 'Asset Name', 'required');
            $this->form_validation->set_rules('a_cost', 'Asset Price', 'required');
            $this->form_validation->set_rules('a_model', 'Asset Model', 'required');
            $this->form_validation->set_rules('a_category', 'Asset Type', 'required');
            $this->form_validation->set_rules('a_supplier_id', 'Supplier', 'required');
            $this->form_validation->set_rules('a_asset_code', 'Asset Code', 'required');
            $this->form_validation->set_rules('a_description', 'Asset Description', 'required');
            $this->form_validation->set_rules('ai_number_in_stock', 'Number in Stock', 'required');
            $this->form_validation->set_rules('a_purchase_date', 'Purchase Date', 'required');

            $data['all_supplier'] = $this->Supplier_Model->get_select_supplier();
            $data['all_asset_type'] = $this->Asset_Category_Model->get_select_asset_category();

            if ($this->form_validation->run() === FALSE) {
                $this->load->view('templates/header', $data);
                $this->load->view('asset/create');
                $this->load->view('templates/footer');
            } else {
                $this->Asset_Model->set_asset();
                $this->session->set_flashdata('asset_add', 'Add Asset Successful');
                redirect('/asset/index', 'refresh');
            }
        } else {
            redirect('login', 'refresh');
        }

    }

    public function create_type()
    {
        if ($this->session->userdata('logged_in')) {
            $data['title'] = 'Create a Asset Type';
            $this->load->helper('form');
            $this->load->library('form_validation');

            $this->form_validation->set_rules('at_name', 'Type Name', 'required');
            $this->form_validation->set_rules('at_description', 'Type Description', 'required');

            if ($this->form_validation->run() === FALSE) {
                $this->load->view('templates/header', $data);
                $this->load->view('asset/create_type');
                $this->load->view('templates/footer');
            } else {
                $this->Asset_Type_Model->set_asset_type();
                $this->session->set_flashdata('type_add', 'Add Asset Type Successful');
                redirect('/asset/type', 'refresh');
            }
        } else {
            redirect('login', 'refresh');
        }
    }

    /**
     * Created by Ha Anh Son
     * @param $id
     */
    public function update($id)
    {
        if ($this->session->userdata('logged_in')) {
            $data['title'] = 'Update a Asset';
            $this->load->helper('form');
            $this->load->library('form_validation');
            $data['success'] = 0;

            $this->form_validation->set_rules('a_name', 'Asset Name', 'required');
            $this->form_validation->set_rules('a_cost', 'Asset Price', 'required');
            $this->form_validation->set_rules('a_model', 'Asset Model', 'required');
            $this->form_validation->set_rules('a_supplier_id', 'Supplier', 'required');
            $this->form_validation->set_rules('a_category', 'Asset Type', 'required');
            $this->form_validation->set_rules('a_description', 'Asset Description', 'required');
            $this->form_validation->set_rules('ai_number_in_stock', 'Number in Stock', 'required');
            $this->form_validation->set_rules('a_purchase_date', 'Purchase Date', 'required');

            $data['all_supplier'] = $this->Supplier_Model->get_select_supplier();
            $data['all_asset_type'] = $this->Asset_Category_Model->get_select_asset_category();
            if ($this->form_validation->run()) {
                $data['success'] = $this->Asset_Model->update_asset($id);
                $this->session->set_flashdata('asset_success', 'Update Asset Successful');
                redirect('/asset/index', 'refresh');
            }

            $data['asset_item'] = $this->Asset_Model->get_asset($id);
            $data['asset_inventory_item'] = $this->Asset_Inventory_Model->get_asset_inventory($id);
            if (empty($data['asset_item'])) {
                show_404();
            }
            $this->load->view('templates/header', $data);
            $this->load->view('asset/update', $data);
            $this->load->view('templates/footer');
        } else {
            redirect('login', 'refresh');
        }

    }

    /**
     * Created by Ha Anh Son
     * @param $id
     */
    public function update_type($id)
    {
        if ($this->session->userdata('logged_in')) {

            $data['title'] = 'Update a Asset Type';
            $this->load->helper('form');
            $this->load->library('form_validation');
            $data['success'] = 0;

            $this->form_validation->set_rules('at_name', 'Type Name', 'required');
            $this->form_validation->set_rules('at_description', 'Type Description', 'required');

            if ($this->form_validation->run()) {
                $data['success'] = $this->Asset_Type_Model->update_asset_type($id);
                $this->session->set_flashdata('type_success', 'Update Asset Type Successful');
                redirect('/asset/type', 'refresh');
            }

            $data['asset_type_item'] = $this->Asset_Type_Model->get_asset_type($id);
            if (empty($data['asset_type_item'])) {
                show_404();
            }
            $this->load->view('templates/header', $data);
            $this->load->view('asset/update_type', $data);
            $this->load->view('templates/footer');
        } else {
            redirect('login', 'refresh');
        }
    }

    /**
     * Created by Ha Anh Son
     * @param $id
     */
    public function update_assigned($id)
    {
        if ($this->session->userdata('logged_in')) {
            $data['title'] = 'Update Assign';
            $this->load->helper('form');
            $this->load->library('form_validation');
            $data['success'] = 0;

            $this->form_validation->set_rules('ea_employee_id', 'Employee', 'required');
            $this->form_validation->set_rules('ea_asset_id', 'Asset', 'required');
            $this->form_validation->set_rules('ea_date_out', 'Date Out', 'required');

            $data['all_employee'] = $this->Employee_Model->get_select_employee();
            $data['all_asset'] = $this->Asset_Model->get_select_asset();

            if ($this->form_validation->run()) {
                $data['success'] = $this->Employee_Asset_Model->update_employee_asset($id);
                $this->session->set_flashdata('employee_asset_success', 'Update Assign Successful');
                redirect('/asset/assigned', 'refresh');
            }

            $data['employee_asset_item'] = $this->Employee_Asset_Model->get_employee_asset($id);
            if (empty($data['employee_asset_item'])) {
                show_404();
            }
            $this->load->view('templates/header', $data);
            $this->load->view('asset/update_assigned', $data);
            $this->load->view('templates/footer');
        } else {
            redirect('login', 'refresh');
        }

    }

    /**
     * Created by Ha Anh Son
     * @param $id
     */
    public function delete_assigned($id)
    {
        if ($this->session->userdata('logged_in')) {
            $this->Employee_Asset_Model->delete_employee_asset($id);
            $this->session->set_flashdata('employee_asset_delete', 'Delete  Successful');
            redirect('/asset/assigned', 'refresh');
        } else {
            redirect('login', 'refresh');
        }

    }

    /**
     * Created by Ha Anh Son
     * @param $id
     */
    public function delete_type($id)
    {
        if ($this->session->userdata('logged_in')) {
            $this->Asset_Type_Model->delete_asset_type($id);
            $this->session->set_flashdata('type_delete', 'Delete Asset Type Successful');
            redirect('/asset/type', 'refresh');
        } else {
            redirect('login', 'refresh');
        }

    }

    /**
     * Created by Ha Anh Son
     * @param $id
     */
    public function delete($id)
    {
        $this->Asset_Model->delete_asset($id);
        $this->session->set_flashdata('asset_delete', 'Delete Asset Successful');
        redirect('/asset/index', 'refresh');
    }

    /**
     * Created by Ha Anh Son
     * @param $id
     */
    public function view_assigned($id)
    {
        if ($this->session->userdata('logged_in')) {
            $data['title'] = 'View assigned';
            $data['employee_asset_info'] = $this->Employee_Asset_Model->get_employee_asset($id);
            $this->load->view('templates/header', $data);
            $this->load->view('asset/view_assigned', $data);
            $this->load->view('templates/footer');
        } else {
            redirect('login', 'refresh');
        }

    }

    /**
     * Created by Ha Anh Son
     * @param $id
     */
    public function view($id)
    {
        if ($this->session->userdata('logged_in')) {
            $data['title'] = 'View a Asset';
            $data['asset_info'] = $this->Asset_Model->get_asset($id);
            $this->load->view('templates/header', $data);
            $this->load->view('asset/view', $data);
            $this->load->view('templates/footer');
        } else {
            redirect('login', 'refresh');
        }

    }

    /**
     * Created by Ha Anh Son
     * @param $id
     */
    public function view_type($id)
    {
        if ($this->session->userdata('logged_in')) {
            $data['title'] = 'View a Asset Type';
            $data['type_info'] = $this->Asset_Type_Model->get_asset_type($id);
            $this->load->view('templates/header', $data);
            $this->load->view('asset/view_type', $data);
            $this->load->view('templates/footer');
        } else {
            redirect('login', 'refresh');
        }

    }


}
