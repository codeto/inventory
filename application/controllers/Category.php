<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller
{

    // Load libraries in Constructor.
    function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        $this->load->helper('breadcrumb_helper');
        $this->load->model('Employee_Model');
        $this->load->model('Employee_Asset_Model');
        $this->load->model('Department_Model');
        $this->load->model('Staff_Model');
        $this->load->model('Asset_Model');
        $this->load->model('Asset_Category_Model');
        $this->load->model('Common_Model');
        $this->load->library('session');
        $this->load->library('pagination');
    }

    public function create()
    {
        if ($this->session->userdata('logged_in')) {
            $data['title'] = 'Create a Category';
            $this->load->helper('form');
            $this->load->library('form_validation');

            $this->form_validation->set_rules('ac_name', 'Category Name', 'required');
            $this->form_validation->set_rules('ac_code', 'Category Code', 'required');
            $this->form_validation->set_rules('ac_remark', 'Category Description', 'required');

            if ($this->form_validation->run() === FALSE) {
                $this->load->view('templates/header', $data);
                $this->load->view('category/create');
                $this->load->view('templates/footer');
            } else {
                $this->Asset_Category_Model->set_asset_category();
                $this->session->set_flashdata('type_add', 'Add Category Successful');
                redirect('/setting/categories', 'refresh');
            }
        } else {
            redirect('login', 'refresh');
        }
    }

    /**
     * Created by Ha Anh Son
     * @param $id
     */
    public function update($id)
    {
        if ($this->session->userdata('logged_in')) {

            $data['title'] = 'Update a Category';
            $this->load->helper('form');
            $this->load->library('form_validation');
            $data['success'] = 0;

            $this->form_validation->set_rules('ac_name', 'Category Name', 'required');
            $this->form_validation->set_rules('ac_code', 'Category Code', 'required');
            $this->form_validation->set_rules('ac_remark', 'Category Description', 'required');

            if ($this->form_validation->run()) {
                $data['success'] = $this->Asset_Category_Model->update_asset_category($id);
                $this->session->set_flashdata('type_success', 'Update Category Successful');
                redirect('/setting/categories', 'refresh');
            }

            $data['asset_type_item'] = $this->Asset_Category_Model->get_asset_category($id);
            if (empty($data['asset_type_item'])) {
                show_404();
            }
            $this->load->view('templates/header', $data);
            $this->load->view('category/update', $data);
            $this->load->view('templates/footer');
        } else {
            redirect('login', 'refresh');
        }
    }
    /**
     * Created by Ha Anh Son
     * @param $id
     */
    public function delete($id)
    {
        if ($this->session->userdata('logged_in')) {
            $this->Asset_Category_Model->delete_asset_category($id);
            $this->session->set_flashdata('staff_delete', 'Delete Category Successful');
            redirect('/setting/categories', 'refresh');
        } else {
            redirect('login', 'refresh');
        }
    }

    public function deleteMultiple() {
        $dat = $this->input->post('category_id');
        for ($i = 0; $i < sizeof($dat); $i++) {
            $category_info = $this->Asset_Category_Model->get_asset_category($dat[$i]);
            if($category_info->ac_qty == 0) {
                $this->Asset_Category_Model->delete_asset_category($dat[$i]);
            }
        }
        redirect('setting/categories', 'refresh');
    }

    /**
     * Created by Ha Anh Son
     * @param $id
     */
    public function view($id)
    {
        if ($this->session->userdata('logged_in')) {
            $data['title'] = 'View a Category';
            $data['type_info'] = $this->Asset_Category_Model->get_asset_category($id);
            $this->load->view('templates/header', $data);
            $this->load->view('category/view', $data);
            $this->load->view('templates/footer');
        } else {
            redirect('login', 'refresh');
        }

    }



}
