<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Department extends CI_Controller
{

    // Load libraries in Constructor.
    function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        $this->load->helper('breadcrumb_helper');
        $this->load->model('Department_Model');
        $this->load->model('Employee_Model');
        $this->load->model('Common_Model');
        $this->load->library('session');
        $this->load->library('session');
        $this->load->library('pagination');
    }

    public function index($page_num = 1, $sortfield = 'de_seq', $order = 'desc')
    {
        if ($this->session->userdata('logged_in')) {
            $data['title'] = 'List Department';
            $data['all_search_field'] = array(
                '' => '--Option--',
                'de_name' => 'Name',
                'de_description' => 'Description'
            );
            $this->load->helper('form');
            $config = $this->Common_Model->getPaging();
            $config["base_url"] = base_url() . "/index.php/department/index";
            $type_search = ($this->input->post("type_search"))? $this->input->post("type_search") : null;
            $keyword = ($this->input->post("search_name"))? $this->input->post("search_name") : null;
            if(!empty($type_search) && !empty($keyword)) {
                $config["base_url"] = base_url() . "/index.php/asset/unassigned/$type_search/$keyword";
            }
            $config["total_rows"] = $this->Department_Model->record_count($type_search, $keyword);

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

            if (empty($page)) $page = 1;
            $offset = ($page - 1) * $config['per_page'];

            $data['page'] = (int)$this->uri->segment(3);
            $order_seg = $this->uri->segment(5, "asc"); // if the 5th segment not present,it will return asc. default value.
            $data["order"] = ($order_seg == "asc") ? 'desc' : 'asc';

            $data["results"] = $this->Department_Model->fetch_department($config["per_page"], $offset, $sortfield, $order, $type_search, $keyword);
            $data["links"] = $this->pagination->create_links();
            $this->load->view('templates/header', $data);
            $this->load->view('department/list');
            $this->load->view('templates/footer');
        } else {
            redirect('login', 'refresh');
        }

    }

    public function create()
    {
        if ($this->session->userdata('logged_in')) {
            $this->load->helper('form');
            $this->load->library('form_validation');

            $data['title'] = 'Create a Department';

            $this->form_validation->set_rules('de_name', 'Name', 'required');
            $this->form_validation->set_rules('de_description', 'Description', 'required');

            if ($this->form_validation->run() === FALSE) {
                $this->load->view('templates/header', $data);
                $this->load->view('department/create');
                $this->load->view('templates/footer');

            } else {
                $this->Department_Model->set_department();
                $this->session->set_flashdata('department_add', 'Add Department Successful');
                redirect('/department/index', 'refresh');
            }
        } else {
            redirect('login', 'refresh');
        }

    }


    /**
     * Created by Ha Anh Son
     * @param $id
     */
    public function update($id)
    {
        if ($this->session->userdata('logged_in')) {
            $this->load->helper('form');
            $this->load->library('form_validation');

            $data['title'] = 'Update a Department';
            $data['success'] = 0;

            $this->form_validation->set_rules('de_name', 'Name', 'required');
            $this->form_validation->set_rules('de_description', 'Description', 'required');

            if ($this->form_validation->run()) {
                $data['success'] = $this->Department_Model->update_department($id);
                $this->session->set_flashdata('department_success', 'Update Department Successful');
                redirect('/department/index', 'refresh');
            }

            $data['department_item'] = $this->Department_Model->get_department($id);
            if (empty($data['department_item'])) {
                show_404();
            }
            $this->load->view('templates/header', $data);
            $this->load->view('department/update', $data);
            $this->load->view('templates/footer');
        } else {
            redirect('login', 'refresh');
        }

    }

    /**
     * Created by Ha Anh Son
     * @param $id
     */
    public function delete($id)
    {
        if ($this->session->userdata('logged_in')) {
            $this->Department_Model->delete_department($id);
            $this->session->set_flashdata('department_delete', 'Delete Department Successful');
            redirect('/department/index', 'refresh');
        } else {
            redirect('login', 'refresh');
        }
    }

    public function deleteMultiple() {
        $dat = $this->input->post('department_id');
        for ($i = 0; $i < sizeof($dat); $i++) {
            $this->Department_Model->delete_department($dat[$i]);
        }
        redirect('department/index', 'refresh');
    }

    /**
     * Created by Ha Anh Son
     * @param $id
     */
    public function view($id)
    {
        if ($this->session->userdata('logged_in')) {
            $data['title'] = 'View a Department';
            $data['department_info'] = $this->Department_Model->get_department($id);
            $this->load->view('templates/header', $data);
            $this->load->view('department/view', $data);
            $this->load->view('templates/footer');
        } else {
            redirect('login', 'refresh');
        }

    }
}
