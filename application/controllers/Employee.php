<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller
{

    // Load libraries in Constructor.
    function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        $this->load->helper('breadcrumb_helper');
        $this->load->model('Employee_Model');
        $this->load->model('Staff_Model');
        $this->load->model('Employee_Asset_Model');
        $this->load->model('Department_Model');
        $this->load->model('Asset_Model');
        $this->load->model('Common_Model');
        $this->load->library('session');
        $this->load->library('pagination');
    }

    public function index($page = 1, $sortfield = 'em_seq', $order = 'desc')
    {
        if ($this->session->userdata('logged_in') || $this->session->userdata('user_logged_in')) {
            $data['title'] = 'List Employee';
            $this->load->helper('form');
            $data['all_search_field'] = array(
                '' => '--Option--',
                'em_email_address' => 'Email',
                'em_cell_mobile' => 'Mobile'
            );
            $config = $this->Common_Model->getPaging(5);
            $config["base_url"] = base_url() . "/index.php/employee/index";
            $config["total_rows"] = $this->Employee_Model->record_count();

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

            if (empty($page)) $page = 1;
            $offset = ($page - 1) * $config['per_page'];

            $data["results"] = $this->Employee_Model->fetch_employee($config["per_page"], $offset, $sortfield, $order);


            $data['page'] = (int)$this->uri->segment(3);
            $order_seg = $this->uri->segment(5, "asc"); // if the 5th segment not present,it will return asc. default value.
            if ($order_seg == "asc") $order = "desc"; else $order = "asc";
            $data["order"] = $order;


            $data["links"] = $this->pagination->create_links();
            $this->load->view('templates/header', $data);
            $this->load->view('employee/list');
            $this->load->view('templates/footer');
        } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }

    public function create()
    {
        if ($this->session->userdata('logged_in')) {
            $this->load->helper('form');
            $this->load->library('form_validation');

            $data['title'] = 'Create a Employee';
            $data['all_department'] = $this->Department_Model->get_select_department();
            $this->form_validation->set_rules('em_first_name', 'Firstname', 'required');
            $this->form_validation->set_rules('em_last_name', 'Lastname', 'required');
            $this->form_validation->set_rules('em_department_id', 'Department', 'required');
            $this->form_validation->set_rules('em_cell_mobile', 'Cellphone', 'required');
            $this->form_validation->set_rules('em_email_address', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('em_other_detail', 'Other', 'required');
            $this->form_validation->set_rules('status', 'Status', 'required');

            if ($this->form_validation->run() === FALSE) {
                $this->load->view('templates/header', $data);
                $this->load->view('employee/create');
                $this->load->view('templates/footer');

            } else {
                $this->Employee_Model->set_employee();
                $this->session->set_flashdata('employee_add', 'Create Employee Successful');
                redirect('/employee/index', 'refresh');
            }
        } else {
            redirect('login', 'refresh');
        }

    }

    /**
     * Created by Ha Anh Son
     * @param $id
     */
    public function update($id)
    {
        if ($this->session->userdata('logged_in')) {
            $this->load->helper('form');
            $this->load->library('form_validation');

            $data['title'] = 'Update a Employee';
            $data['success'] = 0;
            $data['all_department'] = $this->Department_Model->get_select_department();
            $this->form_validation->set_rules('em_first_name', 'Firstname', 'required');
            $this->form_validation->set_rules('em_last_name', 'Lastname', 'required');
            $this->form_validation->set_rules('em_department_id', 'Department', 'required');
            $this->form_validation->set_rules('em_cell_mobile', 'Cellphone', 'required');
            $this->form_validation->set_rules('em_email_address', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('em_other_detail', 'Other', 'required');
            $this->form_validation->set_rules('status', 'Status', 'required');

            if ($this->form_validation->run()) {
                $data['success'] = $this->Employee_Model->update_employee($id);
                $this->session->set_flashdata('employee_success', 'Update Employee Successful');
                redirect('/employee/index', 'refresh');
            }
            $data['employee_item'] = $this->Employee_Model->get_employee($id);
            if (empty($data['employee_item'])) {
                show_404();
            }
            $this->load->view('templates/header', $data);
            $this->load->view('employee/update', $data);
            $this->load->view('templates/footer');
        } else {
            redirect('login', 'refresh');
        }

    }

    /**
     * Created by Ha Anh Son
     * @param $id
     */
    public function delete($id)
    {
        if ($this->session->userdata('logged_in')) {
            $this->Employee_Model->delete_employee($id);
            $this->session->set_flashdata('employee_delete', 'Delete Employee Successful');
            redirect('/employee/index', 'refresh');
        } else {
            redirect('login', 'refresh');
        }
    }

    public function deleteMultiple() {
        $dat = $this->input->post('employee_id');
        for ($i = 0; $i < sizeof($dat); $i++) {
            $this->Employee_Model->delete_employee($dat[$i]);
        }
        redirect('employee/index', 'refresh');
    }

    /**
     * Created by Ha Anh Son
     * @param $id
     */
    public function view($id, $page = 1, $sortfield = 'em_seq', $order = 'asc')
    {
        if ($this->session->userdata('logged_in')) {
            $data['title'] = 'View a Employee';
            $data['employee_info'] = $this->Employee_Model->get_employee($id);
            $department = $this->Department_Model->get_department($data['employee_info']->em_department_id);
            $data['department_name'] = isset($department) ? $department->de_name : '';

            $config = $this->Common_Model->getPaging(20);
            $config["base_url"] = base_url() . "/index.php/staff/view/" . $id;
            $config["total_rows"] = $this->Employee_Asset_Model->record_count_by_employee($id);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $config["uri_segment"] = 6;
            if (empty($page)) $page = 1;
            $offset = ($page - 1) * $config['per_page'];
            $data['page'] = (int)$this->uri->segment(4);
            $order_seg = $this->uri->segment(6, "asc");
            $data["order"] = ($order_seg == "asc") ? 'desc' : 'asc';

            $data["results"] = $this->Employee_Asset_Model->fetch_employee_view($id, $config["per_page"], $offset, $sortfield, $order);
            $this->pagination->initialize($config);
            $data["links"] = $this->pagination->create_links();

            $this->load->view('templates/header', $data);
            $this->load->view('employee/view', $data);
            $this->load->view('templates/footer');
        } else {
            redirect('login', 'refresh');
        }
    }

    function oldpass_check($oldpass)
    {
        $session_data = $this->session->userdata('logged_in');
        $result = $this->Employee_Model->check_oldpassword($oldpass,$session_data['id']);
        if($result ==0)
        {
            $this->form_validation->set_message('oldpass_check', "%s doesn't match.");
            return FALSE ;

        }
        else
        {
            return TRUE ;

        }
    }

    public function changePass() {
        if ($this->session->userdata('user_logged_in')) {
            $this->load->helper('form');
            $this->load->library('form_validation');
            $session_data = $this->session->userdata('user_logged_in');
            $id = $session_data['id'];

            $data['title'] = 'Update a Employee';
            $data['success'] = 0;
            $data['employee_info'] = $this->Employee_Model->get_employee($id);
            $department = $this->Department_Model->get_department($data['employee_info']->em_department_id);
            $data['department_name'] = isset($department) ? $department->de_name : '';

            $this->form_validation->set_rules('current_password', 'Current Password', 'required|callback_oldpass_check');
            $this->form_validation->set_rules('new_password', 'New Password', 'required');

            if ($this->form_validation->run()) {
                $data['success'] = $this->Employee_Model->update_password($id);
                $this->session->set_flashdata('employee_success', 'Update Employee Successful');
                redirect('/employee/index', 'refresh');
            }
            $data['employee_item'] = $this->Employee_Model->get_employee($id);
            if (empty($data['employee_item'])) {
                show_404();
            }
            $this->load->view('templates/header', $data);
            $this->load->view('employee/info', $data);
            $this->load->view('templates/footer');
        } else {
            redirect('login', 'refresh');
        }
    }

    public function info($page = 1, $sortfield = 's_seq', $order = 'asc')
    {
        if ($this->session->userdata('user_logged_in') ) {
            $session_data = $this->session->userdata('user_logged_in');
            $id = $session_data['id'];

            $this->load->helper('form');
            $data['title'] = 'View info';
            $data['employee_info'] = $this->Staff_Model->get_staff($id);
            $department = $this->Department_Model->get_department($data['employee_info']->s_department_id);
            $data['department_name'] = isset($department) ? $department->de_name : '';

            $config = $this->Common_Model->getPaging();
            $config["base_url"] = base_url() . "/index.php/employee/view/" . $id;
            $config["total_rows"] = $this->Employee_Asset_Model->record_count_by_staff($id);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $config["uri_segment"] = 4;
            if (empty($page)) $page = 1;
            $offset = ($page - 1) * $config['per_page'];
            $data['page'] = (int)$this->uri->segment(4);
            $order_seg = $this->uri->segment(6, "asc"); 
            $data["order"] = ($order_seg == "asc") ? 'desc' : 'asc';

            $data["results"] = $this->Employee_Asset_Model->fetch_staff_view($id, $config["per_page"], $offset, $sortfield, $order);
            $this->pagination->initialize($config);
            $data["links"] = $this->pagination->create_links();

            $this->load->view('templates/header', $data);
            $this->load->view('employee/info', $data);
            $this->load->view('templates/footer');

        } else {
            redirect('login', 'refresh');
        }
    }

}
