<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Info extends CI_Controller
{

    // Load libraries in Constructor.
    function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        $this->load->model('Employee_Model');
        $this->load->model('Employee_Asset_Model');
        $this->load->model('Department_Model');
        $this->load->model('Asset_Model');
        $this->load->model('Common_Model');
        $this->load->library('session');
        $this->load->library('pagination');
    }

    public function index($page = 1, $sortfield = 'em_seq', $order = 'asc')
    {
        if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $id = $session_data['id'];

            $this->load->helper('form');
            $data['title'] = 'View info';
            $data['employee_info'] = $this->Employee_Model->get_employee($id);
            $department = $this->Department_Model->get_department($data['employee_info']->em_department_id);
            $data['department_name'] = isset($department) ? $department->de_name : '';

            $config = $this->Common_Model->getPaging();
            $config["base_url"] = base_url() . "/index.php/employee/view/" . $id;
            $config["total_rows"] = $this->Employee_Asset_Model->record_count_by_employee($id);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $config["uri_segment"] = 4;
            if (empty($page)) $page = 1;
            $offset = ($page - 1) * $config['per_page'];
            $data['page'] = (int)$this->uri->segment(4);
            $order_seg = $this->uri->segment(6, "asc"); // if the 5th segment not present,it will return asc. default value.
            $data["order"] = ($order_seg == "asc") ? 'desc' : 'asc';

            $data["results"] = $this->Employee_Asset_Model->fetch_employee_view($id, $config["per_page"], $offset, $sortfield, $order);
            $this->pagination->initialize($config);
            $data["links"] = $this->pagination->create_links();

            $this->load->view('templates/header', $data);
            $this->load->view('info/view', $data);
            $this->load->view('templates/footer');

        } else {
            redirect('login', 'refresh');
        }
    }
}
