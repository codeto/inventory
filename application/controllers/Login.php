<?php
/**
 * Created by Ha Anh Son
 * User: heyso
 * Date: 4/7/2016
 * Time: 9:45 AM
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $this->load->helper(array('form'));
        $this->load->view('login');
    }

    function logout()
    {
        $this->session->unset_userdata('logged_in');
        session_destroy();
        redirect('/login/index', 'refresh');
    }

}

?>