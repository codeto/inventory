<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        $this->load->helper('breadcrumb_helper');
        $this->load->model('Common_Model');
        $this->load->model('Asset_Category_Model');
        $this->load->model('Staff_Model');
        $this->load->model('Department_Model');
        $this->load->library('session');
        $this->load->library('pagination');
    }
    public function categories($page_num = 1, $sortfield = 'ac_seq', $order = 'desc')
    {
        if ($this->session->userdata('logged_in')) {

            $this->load->helper('form');
            $data['title'] = 'List of Categories';
            $data['all_search_field'] = array(
                '' => '--Option--',
                'ac_name' => 'Name',
                'ac_code' => 'Code',
                'ac_remark' => 'Remark'
            );
            $config = $this->Common_Model->getPaging(5);
            $config["base_url"] = base_url() . "/index.php/setting/categories";
            $type_search = ($this->input->post("type_search"))? $this->input->post("type_search") : null;
            $keyword = ($this->input->post("search_name"))? $this->input->post("search_name") : null;
            if(!empty($type_search) && !empty($keyword)) {
                $config["base_url"] = base_url() . "/index.php/setting/categories/$type_search/$keyword";
            }
            $config["total_rows"] = $this->Asset_Category_Model->record_count($type_search, $keyword);

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

            if (empty($page)) $page = 1;
            $offset = ($page - 1) * $config['per_page'];
            $data['page'] = (int)$this->uri->segment(3);
            $order_seg = $this->uri->segment(5, "asc"); // if the 5th segment not present,it will return asc. default value.
            $data["order"] = ($order_seg == "asc") ? 'desc' : 'asc';

            $data["results"] = $this->Asset_Category_Model->fetch_asset_category($config["per_page"], $offset, $sortfield, $order, $type_search, $keyword);
            $data["links"] = $this->pagination->create_links();
            $this->load->view('templates/header', $data);
            $this->load->view('setting/categories');
            $this->load->view('templates/footer');
        } else {
            redirect('login', 'refresh');
        }
    }

    public function staffs($page_num = 1, $sortfield = 's_seq', $order = 'desc')
    {
        if ($this->session->userdata('logged_in')) {
            $this->load->helper('form');
            $data['title'] = 'List of Staffs';
            $data['all_search_field'] = array(
                '' => '--Option--',
                's_name' => 'Name',
                's_userid' => 'Email',
                's_remark' => 'Remark'
            );
            $config = $this->Common_Model->getPaging(5);
            $config["base_url"] = base_url() . "/index.php/setting/staffs";
            $type_search = ($this->input->post("type_search"))? $this->input->post("type_search") : null;
            $keyword = ($this->input->post("search_name"))? $this->input->post("search_name") : null;
            if(!empty($type_search) && !empty($keyword)) {
                $config["base_url"] = base_url() . "/index.php/setting/staffs/$type_search/$keyword";
            }
            $total_row = $this->Staff_Model->record_count($type_search, $keyword);
            $config["total_rows"] = $total_row;

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

            if (empty($page)) $page = 1;
            $offset = ($page - 1) * $config['per_page'];
            $data['page'] = (int)$this->uri->segment(3);
            $order_seg = $this->uri->segment(5, "asc"); // if the 5th segment not present,it will return asc. default value.
            $data["order"] = ($order_seg == "asc") ? 'desc' : 'asc';
            $data["results"] = $this->Staff_Model->fetch_staffs($config["per_page"], $offset, $sortfield, $order , $type_search, $keyword);
            $data["links"] = $this->pagination->create_links();

            $this->load->view('templates/header', $data);
            $this->load->view('setting/staffs');
            $this->load->view('templates/footer');
        } else {
            redirect('login', 'refresh');
        }
    }
}
