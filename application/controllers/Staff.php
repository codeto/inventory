<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends CI_Controller
{

    // Load libraries in Constructor.
    function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        $this->load->helper('breadcrumb_helper');
        $this->load->model('Employee_Model');
        $this->load->model('Employee_Asset_Model');
        $this->load->model('Department_Model');
        $this->load->model('Staff_Model');
        $this->load->model('Asset_Model');
        $this->load->model('Common_Model');
        $this->load->library('session');
        $this->load->library('pagination');
    }

    public function create()
    {
        if ($this->session->userdata('logged_in')) {
            $this->load->helper('form');
            $this->load->library('form_validation');

            $data['title'] = 'Create a Staff';
            $data['all_department'] = $this->Department_Model->get_select_department();
            $this->form_validation->set_rules('s_type', 'Type', 'required');
            $this->form_validation->set_rules('s_department_id', 'Department', 'required');

            $this->form_validation->set_rules('s_userid', 'ID', 'required|valid_email|is_unique[staffs.s_userid]');
            $this->form_validation->set_rules('s_password', 'Password', 'required|matches[conf_password]');
            $this->form_validation->set_rules('conf_password', 'Confirm Password', 'required');
            $this->form_validation->set_rules('s_name', 'Name', 'required');
            $this->form_validation->set_rules('s_tel', 'Tel', 'required');
            $this->form_validation->set_rules('s_remark', 'Remarks', 'required');
            $this->form_validation->set_rules('status', 'Status', 'required');
            
            if ($this->form_validation->run() === FALSE) {
                $this->load->view('templates/header', $data);
                $this->load->view('staff/create');
                $this->load->view('templates/footer');

            } else {
                $this->Staff_Model->set_staff();
                $this->session->set_flashdata('staff_add', 'Create Staff Successful');
                redirect('/setting/staffs', 'refresh');
            }
        } else {
            redirect('login', 'refresh');
        }

    }

    /**
     * Created by Ha Anh Son
     * @param $id
     */
    public function update($id)
    {
        if ($this->session->userdata('logged_in')) {
            $this->load->helper('form');
            $this->load->library('form_validation');

            $data['title'] = 'Update a Staff';
            $data['success'] = 0;
            $data['all_department'] = $this->Department_Model->get_select_department();
            $this->form_validation->set_rules('s_type', 'Type', 'required');
            $this->form_validation->set_rules('s_department_id', 'Department', 'required');
            $this->form_validation->set_rules('s_userid', 'ID', 'required|valid_email');
            $this->form_validation->set_rules('s_password', 'Password', 'required|matches[conf_password]|callback_check_password['.$id.']');
            $this->form_validation->set_rules('conf_password', 'Confirm Password', 'required');
            $this->form_validation->set_rules('s_name', 'Name', 'required');
            $this->form_validation->set_rules('s_tel', 'Tel', 'required');
            $this->form_validation->set_rules('s_remark', 'Remarks', 'required');
            $this->form_validation->set_rules('status', 'Status', 'required');

            if ($this->form_validation->run()) {
                $data['success'] = $this->Staff_Model->update_staff($id);
                $this->session->set_flashdata('staff_success', 'Update Staff Successful');
                redirect('/setting/staffs', 'refresh');
            }
            $data['employee_item'] = $this->Staff_Model->get_staff($id);
            if (empty($data['employee_item'])) {
                show_404();
            }
            $this->load->view('templates/header', $data);
            $this->load->view('staff/update', $data);
            $this->load->view('templates/footer');
        } else {
            redirect('login', 'refresh');
        }
    }

    function check_password($password, $staff_id)
    {
        $result = $this->Staff_Model->same_password($password,$staff_id);
        if($result == true)
        {
            $this->form_validation->set_message('check_password', "%s can't match with old password");
            return false;
        }
        return true;
    }

    /**
     * Created by Ha Anh Son
     * @param $id
     */
    public function delete($id)
    {
        if ($this->session->userdata('logged_in')) {
            $this->Staff_Model->delete_staff($id);
            $this->session->set_flashdata('staff_delete', 'Delete Staff Successful');
            redirect('/setting/staffs', 'refresh');
        }
        redirect('login', 'refresh');
    }

    /**
     * Created by Ha Anh Son
     * @param $id
     */
    public function view($id, $page = 1, $sortfield = 's_seq', $order = 'asc')
    {
        if ($this->session->userdata('logged_in')) {
            $data['title'] = 'View a Employee';
            $data['employee_info'] = $this->Staff_Model->get_staff($id);
            $department = $this->Department_Model->get_department($data['employee_info']->s_department_id);
            $data['department_name'] = isset($department) ? $department->de_name : '';

            $config = $this->Common_Model->getPaging(20);
            $config["base_url"] = base_url() . "/index.php/staff/view/" . $id;
            $config["total_rows"] = $this->Employee_Asset_Model->record_count_by_staff($id);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $config["uri_segment"] = 6;
            if (empty($page)) $page = 1;
            $offset = ($page - 1) * $config['per_page'];
            $data['page'] = (int)$this->uri->segment(4);
            $order_seg = $this->uri->segment(6, "asc");
            $data["order"] = ($order_seg == "asc") ? 'desc' : 'asc';

            $data["results"] = $this->Employee_Asset_Model->fetch_staff_view($id, $config["per_page"], $offset, $sortfield, $order);
            $this->pagination->initialize($config);
            $data["links"] = $this->pagination->create_links();

            $this->load->view('templates/header', $data);
            $this->load->view('staff/view', $data);
            $this->load->view('templates/footer');
        } else {
            redirect('login', 'refresh');
        }
    }


    public function deleteMultiple() {
        $dat = $this->input->post('staff');
        for ($i = 0; $i < sizeof($dat); $i++) {
            $this->Staff_Model->delete_staff($dat[$i]);
        }
        redirect('setting/staffs', 'refresh');
    }

}
