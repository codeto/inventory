<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller
{

    // Load libraries in Constructor.
    function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        $this->load->helper('breadcrumb_helper');
        $this->load->model('Supplier_Model');
        $this->load->model('Common_Model');
        $this->load->library('session');
        $this->load->library('pagination');
    }

    public function index($page_num = 1, $sortfield = 'su_seq', $order = 'desc')
    {
        if ($this->session->userdata('logged_in')) {
            $data['title'] = 'List Suppliers';
            $data['all_search_field'] = array(
                '' => '--Option--',
                'su_name' => 'Name',
                'su_description' => 'Description'
            );
            $this->load->helper('form');
            $config = $this->Common_Model->getPaging();
            $config["base_url"] = base_url() . "/index.php/supplier/index";
            $type_search = ($this->input->post("type_search"))? $this->input->post("type_search") : null;
            $keyword = ($this->input->post("search_name"))? $this->input->post("search_name") : null;
            if(!empty($type_search) && !empty($keyword)) {
                $config["base_url"] = base_url() . "/index.php/supplier/index/$type_search/$keyword";
            }
            $config["total_rows"] = $this->Supplier_Model->record_count($type_search, $keyword);

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            if (empty($page)) $page = 1;
            $offset = ($page - 1) * $config['per_page'];

            $data['page'] = (int)$this->uri->segment(3);
            $order_seg = $this->uri->segment(5, "asc"); // if the 5th segment not present,it will return asc. default value.
            $data["order"] = ($order_seg == "asc") ? 'desc' : 'asc';
            $data["results"] = $this->Supplier_Model->fetch_supplier($config["per_page"], $offset, $sortfield, $order, $type_search, $keyword);
            $data["links"] = $this->pagination->create_links();

            $this->load->view('templates/header', $data);
            $this->load->view('supplier/list');
            $this->load->view('templates/footer');
        } else {
            redirect('login', 'refresh');
        }

    }

    public function create()
    {
        if ($this->session->userdata('logged_in')) {
            $this->load->helper('form');
            $this->load->library('form_validation');

            $data['title'] = 'Create a Supplier';

            $this->form_validation->set_rules('su_name', 'Name', 'required');
            $this->form_validation->set_rules('su_description', 'Description', 'required');
            $this->form_validation->set_rules('status', 'Status', 'required');

            if ($this->form_validation->run() === FALSE) {
                $this->load->view('templates/header', $data);
                $this->load->view('supplier/create');
                $this->load->view('templates/footer');

            } else {
                $this->Supplier_Model->set_supplier();
                $this->session->set_flashdata('supplier_add', 'Create Supplier Successful');
                redirect('/supplier/index', 'refresh');
            }
        } else {
            redirect('login', 'refresh');
        }

    }

    /**
     * Created by Ha Anh Son
     * @param $id
     */
    public function update($id)
    {
        if ($this->session->userdata('logged_in')) {
            $this->load->helper('form');
            $this->load->library('form_validation');

            $data['title'] = 'Update a Supplier';
            $data['success'] = 0;

            $this->form_validation->set_rules('su_name', 'Name', 'required');
            $this->form_validation->set_rules('su_description', 'Description', 'required');
            $this->form_validation->set_rules('status', 'Status', 'required');

            if ($this->form_validation->run()) {
                $data['success'] = $this->Supplier_Model->update_supplier($id);
                $this->session->set_flashdata('supplier_success', 'Update Supplier Successful');
                redirect('/supplier/index', 'refresh');
            }
            $data['supplier_item'] = $this->Supplier_Model->get_supplier($id);
            if (empty($data['supplier_item'])) {
                show_404();
            }
            $this->load->view('templates/header', $data);
            $this->load->view('supplier/update', $data);
            $this->load->view('templates/footer');
        } else {
            redirect('login', 'refresh');
        }

    }

    /**
     * Created by Ha Anh Son
     * @param $id
     */
    public function delete($id)
    {
        if ($this->session->userdata('logged_in')) {
            $this->Supplier_Model->delete_supplier($id);
            $this->session->set_flashdata('supplier_delete', 'Delete Supplier Successful');
            redirect('/supplier/index', 'refresh');
        } else {
            redirect('login', 'refresh');
        }

    }

    public function deleteMultiple() {
        $dat = $this->input->post('supplier_id');
        for ($i = 0; $i < sizeof($dat); $i++) {
            $this->Supplier_Model->delete_supplier($dat[$i]);
        }
        redirect('supplier/index', 'refresh');
    }

    /**
     * Created by Ha Anh Son
     * @param $id
     */
    public function view($id)
    {
        if ($this->session->userdata('logged_in')) {
            $data['title'] = 'View a Supplier';
            $data['supplier_info'] = $this->Supplier_Model->get_supplier($id);
            $this->load->view('templates/header', $data);
            $this->load->view('supplier/view', $data);
            $this->load->view('templates/footer');
        } else {
            redirect('login', 'refresh');
        }

    }

}
