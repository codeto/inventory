<?php
/**
 * Created by Ha Anh Son
 * User: heyso
 * Date: 4/7/2016
 * Time: 9:47 AM
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

class VerifyLogin extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Employee_Model', '', TRUE);
        $this->load->model('Admin_Model', '', TRUE);
        $this->load->model('Staff_Model', '', TRUE);
    }

    function index()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('login');
        } else {
            redirect('/asset/member', 'refresh');
        }
    }

    function admin()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_admin');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('admin_login');
        } else {
            redirect('/asset/index', 'refresh');
        }
    }

    function check_database($password)
    {
        $username = $this->input->post('username');
        $result = $this->Staff_Model->login($username, $password);
        if ($result) {
            $session_data = array(
                'name' => 'session_member',
                'id' => $result->s_seq,
                'username' => $result->s_userid
            );
            $this->session->set_userdata('user_logged_in', $session_data);
            return TRUE;
        } else {
            $this->form_validation->set_message('check_database', 'Invalid username or password');
            return false;
        }
    }

    function check_admin($password)
    {
        $username = $this->input->post('username');
        $result = $this->Staff_Model->admin_login($username, $password);

        if ($result) {
            $session_data = array(
                'name' => 'session_admin',
                'id' => $result->s_seq,
                'username' => $result->s_userid
            );
            $this->session->set_userdata('logged_in', $session_data);

            return TRUE;
        } else {
            $this->form_validation->set_message('check_admin', 'Invalid username or password');
            return false;
        }
    }
}

?>