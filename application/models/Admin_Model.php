<?php

class Admin_Model extends CI_Model
{
    public function record_count()
    {
        return $this->db->count_all("admin");
    }

    function login($username, $password)
    {
        $this->db->select('ad_seq, username, password');
        $this->db->from('admin');
        $this->db->where('username', $username);
        $this->db->where('password', md5($password));
        $this->db->limit(1);

        $query = $this ->db->get();

        if($query->num_rows() == 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }
}

?>
