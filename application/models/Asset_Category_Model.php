<?php
class Asset_Category_Model extends CI_Model {

	public function record_count($type = null, $keyword = null) {
        if(!empty($type) && !empty($keyword)) {
            $this->db->like($type, $keyword);
            $this->db->from('asset_category');
            return $this->db->count_all_results();
        } else {
		    return $this->db->count_all("asset_category");
        }
	}

    public function fetch_asset_category($limit, $start, $sortfield = null, $order = null, $type = null, $keyword = null) {
        $this->db->limit($limit, $start);
        if(!empty($type) && !empty($keyword)) {
            $this->db->like($type, $keyword);
        }
        $this->db->order_by("$sortfield", "$order");
        $query = $this->db->get("asset_category");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

	public function get_all_asset_category()
	{
		$query = $this->db->get('asset_category');
		return $query->result();
	}

    function get_asset_category($id){
        $this->db->select('*');
        $this->db->from('asset_category');
        $this->db->where('ac_seq', $id);
        $query = $this->db->get();
        $result = $query->first_row();
        return $result;
    }

    function get_asset_category_name($id){
        $this->db->select('ac_name');
        $this->db->from('asset_category');
        $this->db->where('ac_seq', $id);
        $query = $this->db->get();
        $result = $query->first_row();
        if($result) {
            return $result->ac_name;
        }
        return false;
    }

    public function get_select_asset_category()
    {
        $data = array();
        $query = $this->db->get('asset_category');
        $data[''] = 'Select Type';
        foreach($query->result() as $key => $value) {
            $data[$value->ac_seq] = $value->ac_name;
        }
        return $data;
    }


    public function set_asset_category()
    {
        $this->load->helper('url');

        $data = array(
            'ac_code' => $this->input->post('ac_code'),
            'ac_name' => $this->input->post('ac_name'),
            'ac_remark' => $this->input->post('ac_remark'),
            'wdate' => time(),
            'status' => $this->input->post('status'),
        );
        return $this->db->insert('asset_category', $data);
    }


    public function update_asset_category($id=0)
    {

        $data = array(
            'ac_code' => $this->input->post('ac_code'),
            'ac_name' => $this->input->post('ac_name'),
            'ac_remark' => $this->input->post('ac_remark'),
            'mdate' => time(),
            'status' => $this->input->post('status'),
        );
        $this->db->where('ac_seq',$id);
        return $this->db->update('asset_category',$data);
    }

//    public function delete_asset_category($id=0)
//    {
//        $this->db->where('ac_seq', $id);
//        return $this->db->delete('asset_category');
//    }

    public function delete_asset_category($id=0)
    {
        $data = array(
            'status' => 0,
        );
        $this->db->where('ac_seq', $id);
        return $this->db->update('asset_category',$data);
    }


}

?>
