<?php
class Staff_Model extends CI_Model {
	public function record_count() {
        if(!empty($type) && !empty($keyword)) {
            $this->db->like($type, $keyword);
            $this->db->from('staffs');
            return $this->db->count_all_results();
        } else {
		    return $this->db->count_all("staffs");
        }
	}

    public function record_count_by_department($department_id)
    {
        $query = $this->db->query('SELECT * FROM staffs WHERE em_department_id = '.$department_id);
        return $query->num_rows();
        return $this->db->count_all("staffs");
    }

    function login($username, $password)
    {
        $this->load->library('encrypt');
//        var_dump($username);
//        var_dump($password);
//        var_dump($this->encrypt->encode($password));
        $this->db->select('s_seq, s_userid, s_password');
        $this->db->from('staffs');
        $this->db->where('s_userid', $username);
//        $this->db->where('s_password', $this->encrypt->encode($password));
//        $this->db->where('s_password', md5($password));
//        $this->db->limit(1);

        $query = $this ->db->get();

        $staff_data = $query->first_row();
        if($staff_data) {
            if($password == $this->encrypt->decode($staff_data->s_password)) {
                return $staff_data;
            }
        }
        return false;
    }


    function admin_login($username, $password)
    {
        $this->load->library('encrypt');
        $this->db->select('s_seq, s_userid, s_password');
        $this->db->from('staffs');
        $this->db->where('s_userid', $username);
        $this->db->where('s_type', 0);

        $query = $this ->db->get();

        $staff_data = $query->first_row();
        if($staff_data) {
            if($password == $this->encrypt->decode($staff_data->s_password)) {
                return $staff_data;
            }
        }
        return false;
    }

   public function fetch_staffs($limit, $start, $sortfield = null, $order = null, $type = null, $keyword = null) {
       $this->db->limit($limit, $start);
       if(!empty($type) && !empty($keyword)) {
           $this->db->like($type, $keyword);
       }
       $this->db->order_by("$sortfield", "$order");
        $query = $this->db->get("staffs");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function get_select_staff()
    {
        $data = array();
        $query = $this->db->get('staffs');
        $data[''] = 'Select Staffs';
        foreach($query->result() as $key => $value) {
            $data[$value->s_seq] = $value->s_name;
        }
        return $data;
    }

	public function get_all_staff()
	{
		$query = $this->db->get('staffs');
		return $query->result();
	}

    function get_staff_name($id){
        $this->db->select('s_name');
        $this->db->from('staffs');
        $this->db->where('s_seq', $id);
        $query = $this->db->get();
        $result = $query->first_row();
        if($result) {
            return $result->s_name;
        }
        return false;
    }

    function get_staff($id){
        $this->db->select('*');
        $this->db->from('staffs');
        $this->db->where('s_seq', $id);
        $query = $this->db->get();
        $result = $query->first_row();
        return $result;
    }

    public function set_staff()
    {
        $this->load->helper('url');
        $this->load->library('encrypt');

        $data = array(
            's_userid' => $this->input->post('s_userid'),
            's_type' => $this->input->post('s_type'),
            's_department_id' => $this->input->post('s_department_id'),
            's_password' => $this->encrypt->encode($this->input->post('s_password')),
            's_name' => $this->input->post('s_name'),
            's_tel' => $this->input->post('s_tel'),
            's_remark' => $this->input->post('s_remark'),
            'wdate' => time(),
            'status' => $this->input->post('status'),
        );

        return $this->db->insert('staffs', $data);
    }

    public function update_staff($id=0)
    {
        $this->load->library('encrypt');
        $data = array(
            's_userid' => $this->input->post('s_userid'),
            's_type' => $this->input->post('s_type'),
            's_department_id' => $this->input->post('s_department_id'),
            's_name' => $this->input->post('s_name'),
            's_tel' => $this->input->post('s_tel'),
            's_remark' => $this->input->post('s_remark'),
            'mdate' => time(),
            'status' => $this->input->post('status'),
        );
        if($this->input->post('s_password') != '') {
            $data['s_password'] = $this->encrypt->encode($this->input->post('s_password'));
        }
        $this->db->where('s_seq',$id);
        return $this->db->update('staffs',$data);
    }

    function same_password($password,$staff_id)
    {
        $this->load->library('encrypt');
        $this->db->where('s_seq', $staff_id);
        $query = $this->db->get('staffs');
        $staff_data = $query->first_row();
        if($password == $this->encrypt->decode($staff_data->s_password)) {
            return true;
        }
        return false;
    }

    public function delete_staff($id=0)
    {
        $this->db->where('s_seq', $id);
        return $this->db->delete('staffs');
    }

    public function update_password($id) {
        $data = array(
            'password' => md5($this->input->post('new_password'))
        );
        $this->db->where('em_seq',$id);
        return $this->db->update('staffs',$data);
    }
}

?>
