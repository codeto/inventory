<?php
class Asset_Inventory_Model extends CI_Model {

    function get_asset_inventory($asset_id){
        $this->db->select('*');
        $this->db->from('asset_inventory');
        $this->db->where('ai_asset_id', $asset_id);
        $query = $this->db->get();
        $result = $query->first_row();
        return $result;
    }

    function update_number_in_stock($asset_id, $action = null) {
        $asset_inventory = $this->get_asset_inventory($asset_id);
        $asset_inventory_update = array(
//            'ai_number_in_stock' => ($action == 'addition') ? intval($asset_inventory->ai_number_in_stock) + 1 : intval($asset_inventory->ai_number_in_stock) - 1,
            'mdate' => time(),
        );
        $this->db->where('ai_asset_id', $asset_id);
        return $this->db->update('asset_inventory', $asset_inventory_update);
    }
}

?>
