<?php

class Asset_Model extends CI_Model
{

    public function record_count()
    {
        if (!empty($type) && !empty($keyword)) {
            $this->db->like($type, $keyword);
            $this->db->from('assets');
            return $this->db->count_all_results();
        } else {
            return $this->db->count_all("assets");
        }
    }

    public function record_count_unassigned()
    {
        $this->db->select('employee_asset.*');
        $this->db->from('employee_asset');
        $query = $this->db->get();
        $result = $query->result();

        $assigned = array();
        foreach ($result as $key => $value) {
            $assigned[] = $value->ea_asset_id;
        }

        $this->db->select('assets.*,assets.status as a_status, asset_inventory.*');
        $this->db->from('assets');
        $this->db->join('asset_inventory','assets.a_seq = asset_inventory.ai_asset_id', 'left');

        $this->db->where_not_in('a_seq', $assigned);
        return $this->db->count_all_results();
    }

    public function fetch_asset($limit, $start, $sortfield = null, $order = null, $type = null, $keyword = null)
    {
        $this->db->select('assets.*,assets.status as a_status,assets.wdate as a_wdate, asset_inventory.*');
        $this->db->from('assets');
        $this->db->join('asset_inventory', 'assets.a_seq = asset_inventory.ai_asset_id', 'left');
        if (!empty($type) && !empty($keyword)) {
            $this->db->like($type, $keyword);
        }
        $this->db->order_by("$sortfield", "$order");
        $this->db->limit($limit, $start);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_select_asset()
    {
        $data = array();
        $query = $this->db->get('assets');
        $data[''] = 'Select Device';
        foreach ($query->result() as $key => $value) {
            $data[$value->a_seq] = $value->a_name;
        }
        return $data;
    }

    public function get_all_asset()
    {
        $query = $this->db->get('assets');
        return $query->result();
    }

    function get_asset_name($id)
    {
        $this->db->select('a_name');
        $this->db->from('assets');
        $this->db->where('a_seq', $id);
        $query = $this->db->get();
        $result = $query->first_row();
        if ($result) {
            return $result->a_name;
        }
        return false;
    }

    function get_asset($id)
    {
        $this->db->select('*');
        $this->db->from('assets');
        $this->db->where('a_seq', $id);
        $query = $this->db->get();
        $result = $query->first_row();
        if ($result) {
            return $result;
        }
        return false;
    }

    public function set_asset()
    {
        $this->load->helper('url');

        $data = array(
            'a_name' => $this->input->post('a_name', true),
            'a_description' => $this->input->post('a_description', true),
            'a_cost' => $this->input->post('a_cost'),
            'a_category' => $this->input->post('a_category'),
            'a_is_expendables' => $this->input->post('a_is_expendables'),
            'a_supplier_id' => $this->input->post('a_supplier_id'),
            'a_model' => $this->input->post('a_model'),
            'a_other_detail' => $this->input->post('a_other_detail'),
            'a_purchase_date' => strtotime($this->input->post('a_purchase_date')),
            'a_asset_code' => $this->input->post('a_asset_code'),
            'wdate' => time(),
            'status' => $this->input->post('status'),
        );

        $this->db->insert('assets', $data);

        //Add asset, increase qty to 1
        $this->load->model('Asset_Category_Model', 'asset_category');
        $category = $this->asset_category->get_asset_category($this->input->post('a_category'));
        $category_update = array(
            'ac_qty' => $category->ac_qty + 1,
            'mdate' => time(),
        );
        $this->db->where('ac_seq', $this->input->post('a_category'));
        $this->db->update('asset_category', $category_update);
        //End add asset

        $data = array(
            'ai_asset_id' => $this->db->insert_id(),
            'ai_number_in_stock' => $this->input->post('ai_number_in_stock'),
            'ai_inventory_date' => $this->input->post('ai_inventory_date'),
            'ai_other_detail' => $this->input->post('ai_other_detail'),
            'wdate' => time(),
            'ai_status' => 1,
        );

        return $this->db->insert('asset_inventory', $data);
    }


    public function update_asset($id = 0)
    {

        $data = array(
            'a_name' => $this->input->post('a_name'),
            'a_description' => $this->input->post('a_description'),
            'a_cost' => $this->input->post('a_cost'),
            'a_category' => $this->input->post('a_category'),
            'a_is_expendables' => $this->input->post('a_is_expendables'),
            'a_supplier_id' => $this->input->post('a_supplier_id'),
            'a_model' => $this->input->post('a_model'),
            'a_other_detail' => $this->input->post('a_other_detail'),
            'a_purchase_date' => strtotime($this->input->post('a_purchase_date')),
            'a_asset_code' => $this->input->post('a_asset_code'),
            'mdate' => time(),
            'status' => $this->input->post('status'),
        );
        $this->db->where('a_seq', $id);

        $this->db->update('assets', $data);

        $data = array(
            'ai_number_in_stock' => intval($this->input->post('ai_number_in_stock')),
            'ai_inventory_date' => $this->input->post('ai_inventory_date'),
            'ai_other_detail' => $this->input->post('ai_other_detail'),
            'wdate' => time(),
            'ai_status' => 1,
        );

        $this->db->where('ai_asset_id', $id);

        return $this->db->update('asset_inventory', $data);
    }

    public function delete_asset($id = 0)
    {
        //Add asset, decrease qty to 1
        $asset_info = $this->get_asset($id);
        $this->load->model('Asset_Category_Model', 'asset_category');
        $category = $this->asset_category->get_asset_category($asset_info->a_category);
        $category_update = array(
            'ac_qty' => $category->ac_qty - 1,
            'mdate' => time(),
        );
        $this->db->where('ac_seq', $asset_info->a_category);
        $this->db->update('asset_category', $category_update);
        //End add asset
        $data = array(
            'status' => 'N',
        );
        $this->db->where('a_seq', $id);
        return $this->db->update('assets', $data);
    }

}

?>
