<?php
class Asset_Type_Model extends CI_Model {

	public function record_count() {
		return $this->db->count_all("asset_types");
	}

    public function fetch_asset_types($limit, $start, $sortfield = null, $order = null) {
        $this->db->limit($limit, $start);
        $this->db->order_by("$sortfield", "$order");
        $query = $this->db->get("asset_types");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

	public function get_all_asset_types()
	{
		$query = $this->db->get('asset_types');
		return $query->result();
	}

    function get_asset_type($id){
        $this->db->select('*');
        $this->db->from('asset_types');
        $this->db->where('at_seq', $id);
        $query = $this->db->get();
        $result = $query->first_row();
        return $result;
    }

    function get_asset_type_name($id){
        $this->db->select('at_name');
        $this->db->from('asset_types');
        $this->db->where('at_seq', $id);
        $query = $this->db->get();
        $result = $query->first_row();
        if($result) {
            return $result->at_name;
        }
        return false;
    }

    public function get_select_asset_type()
    {
        $data = array();
        $query = $this->db->get('asset_types');
        $data[''] = 'Select Type';
        foreach($query->result() as $key => $value) {
            $data[$value->at_seq] = $value->at_name;
        }
        return $data;
    }


    public function set_asset_type()
    {
        $this->load->helper('url');

        $data = array(
            'at_name' => $this->input->post('at_name'),
            'at_description' => $this->input->post('at_description'),
            'wdate' => time(),
            'status' => 1,
        );
        return $this->db->insert('asset_types', $data);
    }


    public function update_asset_type($id=0)
    {

        $data = array(
            'at_name'	=>	$this->input->post('at_name'),
            'at_description' =>	$this->input->post('at_description'),
            'mdate' => time(),
            'status' =>	1
        );
        $this->db->where('at_seq',$id);
        return $this->db->update('asset_types',$data);
    }

    public function delete_asset_type($id=0)
    {
        $this->db->where('at_seq', $id);
        return $this->db->delete('asset_types');
    }

}

?>
