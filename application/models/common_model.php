<?php
/**
 * Created by Ha Anh Son
 * User: heyso
 * Date: 3/30/2016
 * Time: 2:01 PM
 */
class Common_Model extends CI_Model {

    public function getStatus($status = null)
    {
        $base = array(
            STATUS_ACTIVE => 'Y',
            STATUS_UNACTIVE => 'N'
        );
        return !empty($base[$status]) ? $base[$status] : 'Undefine';
    }

    public function getStaffType($status = null)
    {
        $base = array(
            0 => 'Admin',
            1 => 'Staff'
        );
        return !empty($base[$status]) ? $base[$status] : 'Undefine';
    }

    public function getStatusAsset($status = null)
    {
        $base = array(
            'N' => 'New',
            'A' => 'Assigned',
            'U' => 'Unassigned',
            'B' => 'Broken',
            'D' => 'Disposed'
        );
        return !empty($base[$status]) ? $base[$status] : 'Undefine';
    }

    public function getPaging($perPage = 10){
        $config = array();
        $config["per_page"] = $perPage;
        $config['use_page_numbers'] = TRUE;
        $config["uri_segment"] = 3;
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';

        //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        return $config;
    }
}