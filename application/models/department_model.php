<?php
class Department_Model extends CI_Model {

	public function record_count($type = null, $keyword = null) {
        if(!empty($type) && !empty($keyword)) {
            $this->db->like($type, $keyword);
            $this->db->from('department');
            return $this->db->count_all_results();
        } else {
		    return $this->db->count_all("department");
        }
	}

    public function fetch_department($limit, $start, $sortfield = null, $order = null, $type = null, $keyword = null) {
        $this->db->order_by("$sortfield", "$order");
        $this->db->limit($limit, $start);
        if(!empty($type) && !empty($keyword)) {
            $this->db->like($type, $keyword);
        }
        $query = $this->db->get("department");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

	public function get_all_department()
	{
		$query = $this->db->get('department');
		return $query->result();
	}

    public function get_select_department()
    {
        $data = array();
        $query = $this->db->get('department');
        $data[''] = 'Select Department';
        foreach($query->result() as $key => $value) {
            $data[$value->de_seq] = $value->de_name;
        }
        return $data;
    }

    function get_department($id){
        $this->db->select('*');
        $this->db->from('department');
        $this->db->where('de_seq', $id);
        $query = $this->db->get();
        $result = $query->first_row();
        return $result;
    }

    function get_department_name($id){
        $this->db->select('de_name');
        $this->db->from('department');
        $this->db->where('de_seq', $id);
        $query = $this->db->get();
        $result = $query->first_row();
        if($result) {
            return $result->de_name;
        }
        return false;
    }

    public function set_department()
    {
        $data = array(
            'de_name' => $this->input->post('de_name'),
            'de_description' => $this->input->post('de_description'),
            'wdate' => time(),
            'status' => 1,
        );

        return $this->db->insert('department', $data);
    }

    public function update_department($id=0)
    {

        $data = array(
            'de_name'	=>	$this->input->post('de_name'),
            'de_description' =>	$this->input->post('de_description'),
            'mdate' => time(),
            'status' =>	1

        );
        $this->db->where('de_seq',$id);
        return $this->db->update('department',$data);
    }

    public function delete_department($id=0)
    {
        $data = array(
            'status' => 0,
        );
        $this->db->where('de_seq', $id);
        return $this->db->update('department',$data);
    }
}

?>
