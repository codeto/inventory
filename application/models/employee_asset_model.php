<?php

class Employee_Asset_Model extends CI_Model
{
    public function record_count($type = null, $keyword = null)
    {
        if (!empty($type) && !empty($keyword)) {
            $this->db->like($type, $keyword);
            $this->db->from('employee_asset');
            return $this->db->count_all_results();
        } else {
            return $this->db->count_all("employee_asset");
        }
    }

    public function record_count_by_employee($employee_id)
    {
        $this->db->where('ea_employee_id', $employee_id);
        return $this->db->count_all("employee_asset");
    }

    public function record_count_by_staff($staff_id)
    {
        $this->db->where('ea_employee_id', $staff_id);
        return $this->db->count_all("employee_asset");
    }

    public function record_count_by_asset($asset_id)
    {
        $query = $this->db->query('SELECT * FROM employee_asset WHERE ea_asset_id=' . $asset_id);
        return $query->num_rows();
        return $this->db->count_all("employee_asset");
    }

    public function fetch_employee_asset($limit, $start, $sortfield = null, $order = null, $employee_id = null, $type = null, $keyword = null)
    {
        $this->db->select('employee_asset.*,employee_asset.status as ea_status, assets.status as a_status, employees.*');
        $this->db->from('employee_asset');
        $this->db->join('employees', 'employee_asset.ea_employee_id = employees.em_seq', 'left');
        $this->db->join('assets', 'employee_asset.ea_asset_id = assets.a_seq', 'left');
        if ($employee_id) {
            $this->db->where('ea_employee_id', $employee_id);
        }
        $this->db->order_by("$sortfield", "$order");
        $this->db->limit($limit, $start);
        if (!empty($type) && !empty($keyword)) {
            $this->db->like($type, $keyword);
        }
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function fetch_unassigned($limit, $start, $sortfield = null, $order = null, $type = null, $keyword = null)
    {

        $this->db->select('employee_asset.*');
        $this->db->from('employee_asset');
        $query = $this->db->get();
        $result = $query->result();

        $assigned = array();
        foreach ($result as $key => $value) {
            $assigned[] = $value->ea_asset_id;
        }

        $this->db->select('assets.*,assets.status as a_status, asset_inventory.*');
        $this->db->from('assets');
        $this->db->join('asset_inventory', 'assets.a_seq = asset_inventory.ai_asset_id', 'left');

        $this->db->where_not_in('a_seq', $assigned);
        $this->db->order_by("$sortfield", "$order");
        $this->db->limit($limit, $start);
        if (!empty($type) && !empty($keyword)) {
            $this->db->like($type, $keyword);
        }
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function fetch_employee_view($employee_id, $limit, $start, $sortfield = null, $order = null)
    {
        $this->db->select('employee_asset.*, employees.*');
        $this->db->from('employee_asset');
        $this->db->join('employees', 'employee_asset.ea_employee_id = employees.em_seq', 'left');
        $this->db->join('assets', 'employee_asset.ea_asset_id = assets.a_seq', 'left');
        $this->db->where('ea_employee_id', $employee_id);
        $this->db->order_by("$sortfield", "$order");
        $this->db->limit($limit, $start);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }

        return false;
    }

    public function fetch_staff_view($employee_id, $limit, $start, $sortfield = null, $order = null)
    {
        $this->db->select('employee_asset.*, staffs.*');
        $this->db->from('employee_asset');
        $this->db->join('staffs', 'employee_asset.ea_employee_id = staffs.s_seq', 'left');
        $this->db->join('assets', 'employee_asset.ea_asset_id = assets.a_seq', 'left');
        $this->db->where('ea_employee_id', $employee_id);
        $this->db->order_by("$sortfield", "$order");
        $this->db->limit($limit, $start);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }

        return false;
    }

    public function get_all_employee_asset()
    {
        $query = $this->db->get('employee_asset');
        return $query->result();
    }

    function get_employee_asset_by_asset_id($id)
    {
        $this->db->select('*');
        $this->db->from('employee_asset');
        $this->db->where('ea_asset_id', $id);
        $query = $this->db->get();
        $result = $query->first_row();
        if ($result) {
            return $result;
        }
        return false;
    }

    function get_employee_asset($id)
    {
        $this->db->select('*');
        $this->db->from('employee_asset');
        $this->db->where('ea_seq', $id);
        $query = $this->db->get();
        $result = $query->first_row();
        if ($result) {
            return $result;
        }
        return false;
    }

    public function getAssignedUser($asset_id)
    {
        $this->db->select('*');
        $this->db->from('employee_asset');
        $this->db->where('ea_asset_id', $asset_id);
        $query = $this->db->get();
        $result = $query->first_row();
        if ($result) {
            $this->load->model('Employee_Model', 'employee_model');
            $employee = $this->employee_model->get_employee($result->ea_employee_id);

            if ($employee) {
                return $employee->em_first_name.' '.$employee->em_last_name;
            } else {
                return '';
            }
        }
        return '';

    }

    public function quick_set_employee_asset()
    {
        $errors = array(
            'error' => 0
        );
        $employee_id = isset($_POST['employee_id']) ? trim($_POST['employee_id']) : '';
        $department_id = isset($_POST['department_id']) ? trim($_POST['department_id']) : '';
        $asset_id = isset($_POST['asset_id']) ? trim($_POST['asset_id']) : '';

        if (empty($department_id)) {
            $errors['department_id'] = 'Department is required';
            $errors['error'] += 1;
        }
        if (empty($employee_id)) {
            $errors['employee_id'] = 'Employee is required';
            $errors['error'] += 1;
        }
        $checkAssigned = $this->check_assigned($asset_id);
        if ($checkAssigned) {
            $errors['exist'] = 'Asset is assigned';
            $errors['error'] += 1;
        }
        if (count($errors) > 0 && $errors['error'] > 0) {
            echo(json_encode($errors));
            die;
        }
//        $this->load->helper('url');
        $this->load->model('Asset_Model', 'asset_model');

        $asset_update = array(
            'status' => 'A',
            'mdate' => time(),
        );
        $this->db->where('a_seq', $asset_id);
        $this->db->update('assets', $asset_update);

        $data = array(
            'ea_employee_id' => $employee_id,
            'ea_asset_id' => $asset_id,
            'wdate' => time(),
            'ea_date_out' => time(),
            'status' => 1,
        );
        $result = $this->db->insert('employee_asset', $data);
        if ($result == false) {
            $errors['database'] = 'Error database';
            $errors['error'] += 1;
        }
        $this->load->model('Asset_Inventory_Model', 'asset_inventory');
//        $this->asset_inventory->update_number_in_stock($this->input->post('asset_id'), 'subtraction');
        echo(json_encode($errors));
        die;
    }

    public function set_employee_asset()
    {
        $this->load->helper('url');
        $data = array(
            'ea_employee_id' => $this->input->post('ea_employee_id'),
            'ea_asset_id' => $this->input->post('ea_asset_id'),
            'ea_date_out' => strtotime($this->input->post('ea_date_out')),
            'ea_date_returned' => strtotime($this->input->post('ea_date_returned')),
            'ea_other_detail' => $this->input->post('ea_other_detail'),
            'ea_condition_out' => $this->input->post('ea_condition_out'),
            'ea_condition_returned' => $this->input->post('ea_condition_returned'),
            'wdate' => time(),
            'status' => $this->input->post('status'),
        );
        $this->db->insert('employee_asset', $data);
        $this->load->model('Asset_Inventory_Model', 'asset_inventory');
        $this->asset_inventory->update_number_in_stock($this->input->post('ea_asset_id'), 'subtraction');
    }


    public function update_employee_asset($id = 0)
    {
        $data_old = $this->get_employee_asset($id);

        $data = array(
            'ea_employee_id' => $this->input->post('ea_employee_id'),
            'ea_asset_id' => $this->input->post('ea_asset_id'),
            'ea_date_out' => strtotime($this->input->post('ea_date_out')),
            'ea_date_returned' => strtotime($this->input->post('ea_date_returned')),
            'ea_other_detail' => $this->input->post('ea_other_detail'),
            'ea_condition_out' => $this->input->post('ea_condition_out'),
            'ea_condition_returned' => $this->input->post('ea_condition_returned'),
            'mdate' => time(),
            'status' => $this->input->post('status'),
        );
        $this->db->where('ea_seq', $id);
        $this->db->update('employee_asset', $data);
        $this->load->model('Asset_Inventory_Model', 'asset_inventory');
        if ($data_old->ea_asset_id != $this->input->post('ea_asset_id')) {
            $this->asset_inventory->update_number_in_stock($data_old->ea_asset_id, 'addition');
            $this->asset_inventory->update_number_in_stock($this->input->post('ea_asset_id'), 'substraction');
        }
    }

    public function delete_employee_asset($id = 0)
    {
        $data = array(
            'status' => 'N',
        );
        $this->db->where('ea_seq', $id);
        return $this->db->update('employee_asset', $data);
    }

    public function unassigned_asset($id = 0, $asset_id = null)
    {
        $data = array(
            'status' => 'U',
        );
        $this->db->where('a_seq', $asset_id);
        $this->db->update('assets', $data);

        $this->db->where('ea_seq', $id);
        return $this->db->delete('employee_asset');
    }

    public function check_assigned($ea_asset_id, $ea_employee_id = null)
    {
        $this->db->from('employee_asset');
        if ($ea_employee_id) {
            $this->db->where('ea_employee_id', $ea_employee_id);
        }
        $this->db->where('ea_asset_id', $ea_asset_id);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }


}

?>
