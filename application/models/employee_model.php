<?php
class Employee_Model extends CI_Model {
	// Count all record of table "contact_info" in database.
	public function record_count() {
		return $this->db->count_all("employees");
	}

    public function record_count_by_department($department_id)
    {
        $query = $this->db->query('SELECT * FROM employees WHERE em_department_id = '.$department_id);
        return $query->num_rows();
    }

    function login($username, $password)
    {
        $this->db->select('em_seq, username, password');
        $this->db->from('employees');
        $this->db->where('username', $username);
        $this->db->where('password', md5($password));
        $this->db->limit(1);

        $query = $this ->db->get();

        if($query->num_rows() == 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }

   public function fetch_employee($limit, $start, $sortfield = null, $order = null) {
        $this->db->limit($limit, $start);
       $this->db->order_by("$sortfield", "$order");
        $query = $this->db->get("employees");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function get_select_employee()
    {
        $data = array();
        $query = $this->db->get('employees');
        $data[''] = 'Select Employee';
        foreach($query->result() as $key => $value) {
            $data[$value->em_seq] = $value->em_first_name.' '.$value->em_last_name;
        }
        return $data;
    }

	public function get_all_employee()
	{
		$query = $this->db->get('employees');
		return $query->result();
	}

    function get_employee_name($id){
        $this->db->select('em_first_name, em_last_name');
        $this->db->from('employees');
        $this->db->where('em_seq', $id);
        $query = $this->db->get();
        $result = $query->first_row();
        if($result) {
            return $result->em_first_name.' '.$result->em_last_name;
        }
        return false;
    }

    function get_employee($id){
        $this->db->select('*');
        $this->db->from('employees');
        $this->db->where('em_seq', $id);
        $query = $this->db->get();
        $result = $query->first_row();
        return $result;
    }

    public function set_employee()
    {
        $this->load->helper('url');

        $data = array(
            'em_first_name' => $this->input->post('em_first_name'),
            'em_last_name' => $this->input->post('em_last_name'),
            'em_department_id' => $this->input->post('em_department_id'),
            'em_cell_mobile' => $this->input->post('em_cell_mobile'),
            'em_email_address' => $this->input->post('em_email_address'),
            'em_other_detail' => $this->input->post('em_other_detail'),
            'wdate' => time(),
            'status' => $this->input->post('status'),
        );

        return $this->db->insert('employees', $data);
    }

    public function update_employee($id=0)
    {

        $data = array(
            'em_first_name' => $this->input->post('em_first_name'),
            'em_last_name' => $this->input->post('em_last_name'),
            'em_department_id' => $this->input->post('em_department_id'),
            'em_cell_mobile' => $this->input->post('em_cell_mobile'),
            'em_email_address' => $this->input->post('em_email_address'),
            'em_other_detail' => $this->input->post('em_other_detail'),
            'mdate' => time(),
            'status' => $this->input->post('status'),
        );
        $this->db->where('em_seq',$id);
        return $this->db->update('employees',$data);
    }

//    public function delete_employee($id=0)
//    {
//        $this->db->where('em_seq', $id);
//        return $this->db->delete('employees');
//    }

    public function delete_employee($id=0)
    {
        $data = array(
            'status' => 0,
        );
        $this->db->where('em_seq', $id);
        return $this->db->update('employees',$data);
    }

    public function update_password($id) {
        $data = array(
            'password' => md5($this->input->post('new_password'))
        );
        $this->db->where('em_seq',$id);
        return $this->db->update('employees',$data);
    }

    function check_oldpassword($oldpass,$user_id)
    {
        $this->db->where('em_seq', $user_id);
        $this->db->where('password', md5($oldpass));
        $query = $this->db->get('employees'); //data table
        return $query->num_rows();
    }
}

?>
