<?php
class Supplier_Model extends CI_Model {

	public function record_count($type = null, $keyword = null) {
        if(!empty($type) && !empty($keyword)) {
            $this->db->like($type, $keyword);
            $this->db->from('supplier');
            return $this->db->count_all_results();
        } else {
		    return $this->db->count_all("supplier");
        }
	}

    public function fetch_supplier($limit, $start, $sortfield = null, $order = null, $type = null, $keyword = null) {
        $this->db->limit($limit, $start);
        if(!empty($type) && !empty($keyword)) {
            $this->db->like($type, $keyword);
        }
        $this->db->order_by("$sortfield", "$order");
        $query = $this->db->get("supplier");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

	public function get_all_supplier()
	{
		$query = $this->db->get('supplier');
		return $query->result();
	}

    public function get_select_supplier()
    {
        $data = array();
        $query = $this->db->get('supplier');
        $data[''] = 'Select Supplier';
        foreach($query->result() as $key => $value) {
            $data[$value->su_seq] = $value->su_name;
        }
        return $data;
    }


    function get_supplier($id){
        $this->db->select('*');
        $this->db->from('supplier');
        $this->db->where('su_seq', $id);
        $query = $this->db->get();
        $result = $query->first_row();
        return $result;
    }

    public function set_supplier()
    {
        $this->load->helper('url');

        $data = array(
            'su_name' => $this->input->post('su_name'),
            'su_description' => $this->input->post('su_description'),
            'wdate' => time(),
            'status' => $this->input->post('status'),
        );
        return $this->db->insert('supplier', $data);
    }


    public function update_supplier($id=0)
    {

        $data = array(
            'su_name'	=>	$this->input->post('su_name'),
            'su_description' =>	$this->input->post('su_description'),
            'mdate' => time(),
            'status' => $this->input->post('status'),
        );
        $this->db->where('su_seq',$id);
        return $this->db->update('supplier',$data);
    }

    public function delete_supplier($id=0)
    {
        $data = array(
            'status' => 0,
        );
        $this->db->where('su_seq', $id);
        return $this->db->update('supplier',$data);
    }

}

?>
