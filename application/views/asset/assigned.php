<?php
$baseUrl = base_url() . 'index.php/';
?>
<h1><?php echo $title; ?></h1>
<?php echo form_open('asset/assigned' , array('class' => 'form-horizontal')); ?>
<div class="input-group" style="margin-bottom: 12px;">
    <div class="input-group-addon">Keyword </div>
    <?php
    echo form_dropdown('type_search', $all_search_field, set_value('type_search'), array('class' => "form-control", "style" => 'width: 7%'));
    ?>
    <input type="text" class="form-control" style="width: 20%" name="search_name" id="search_name" value="<?php echo set_value('search_name'); ?>">
    <input type="submit" name="submit" class="btn btn-primary" value="Search">
</div>
</form>
<?php echo form_open('asset/deleteMultipleAssigned' , array('class' => 'form-horizontal')); ?>
<?php if ($this->session->flashdata('employee_asset_success')) { ?>
    <br>
    <div class="alert alert-success alert-dismissible" role="alert" style="margin-top: 20px;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
        <?php echo $this->session->flashdata('employee_asset_success'); ?>
    </div>
<?php } ?>
<?php if ($this->session->flashdata('employee_asset_add')) { ?>
    <br>
    <div class="alert alert-success alert-dismissible" role="alert" style="margin-top: 20px;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
        <?php echo $this->session->flashdata('employee_asset_add'); ?>
    </div>
<?php } ?>
<?php if ($this->session->flashdata('employee_asset_delete')) { ?>
    <br>
    <div class="alert alert-success alert-dismissible" role="alert" style="margin-top: 20px;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
        <?php echo $this->session->flashdata('employee_asset_delete'); ?>
    </div>
<?php } ?>
<input type="submit" name="submit" class="btn btn-danger" value="Delete Selected Items">
<a class="btn btn-success pull-right" href="<?php echo $baseUrl . "asset/create_assigned"; ?>">Create</a>
<hr>
<div id="form_input">
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th style="width: 4%"><a
                        href="<?php echo base_url(); ?>asset/assigned/<?= $page ?>/ea_seq/<?= $order ?>"><i
                            class="fa fa-fw fa-sort"></i>ID</a></th>
                <th>Asset Name</th>
                <th>Asset ID</th>
                <th>Employee</th>
                <th>Date Out</th>
                <th>Date Returned</th>
                <th>Condition Out</th>
                <th>Condition Returned</th>
                <th>Status</th>
                <th style="width: 15%">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php if (!empty($results)) : ?>
                <?php foreach ($results as $data) { ?>
                    <tr>
                        <td><input type="checkbox" name="asset_id[]" value="<?php echo $data->ea_seq; ?>"><a href=""><?php echo $data->ea_seq; ?></a></td>
                        <td><?php echo $this->Asset_Model->get_asset_name($data->ea_asset_id); ?></td>
                        <td><?php echo $data->ea_asset_id; ?></td>
                        <td><?php echo $this->Employee_Model->get_employee_name($data->ea_employee_id); ?></td>
                        <td><?php echo date('Y:m:d h:i:s', $data->ea_date_out); ?></td>
                        <td><?php echo (isset($data->ea_date_returned) && $data->ea_date_returned != 0) ? date('Y:m:d h:i:s', $data->ea_date_returned) : ''; ?></td>
                        <td><?php echo $data->ea_condition_out; ?></td>
                        <td><?php echo $data->ea_condition_returned; ?></td>
                        <td><?php echo $this->Common_Model->getStatusAsset($data->a_status);?></td>
                        <td class="text-left">
                            <a class="btn btn-default"
                               href="<?php echo $baseUrl . "asset/view_assigned/" . $data->ea_seq; ?>">View</a>
                            <a class="btn btn-primary"
                               href="<?php echo $baseUrl . "asset/update_assigned/" . $data->ea_seq; ?>">Update</a>
                            <a class="btn btn-danger"
                               href="<?php echo $baseUrl . "asset/delete_assigned/" . $data->ea_seq; ?>">Delete</a>
                        </td>
                    </tr>
                <?php } ?>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>
<div class="text-center">
    <p><?php echo $links; ?></p>
</div>