<?php
$baseUrl = base_url().'index.php/';
?>
<h1><?php echo $title; ?></h1>
<?php if(validation_errors() != false) { ?>
	<div class="alert alert-danger" role="alert">
		<?php echo validation_errors(); ?>	
	</div>
<?php } ?>
<?php echo form_open('asset/create'); ?>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group <?php echo form_error('a_name') ? 'has-error': ''; ?>">
                <label for="title">Name</label>
                <input type="text" class="form-control" name="a_name" id="a_name" value="<?php echo set_value('a_name'); ?>" placeholder="Asset Name">
            </div>
            <div class="form-group <?php echo form_error('a_category') ? 'has-error': ''; ?>">
                <label for="title">Asset Category</label>
                <?php
                echo form_dropdown('a_category', $all_asset_type, set_value('a_category'), 'class="form-control"');
                ?>
            </div>
            <div class="form-group <?php echo form_error('a_is_expendables') ? 'has-error': ''; ?>">
                <label for="title">Is Expendables</label>
                <input type="text" class="form-control" name="a_is_expendables" id="a_is_expendables" value="<?php echo set_value('a_is_expendables'); ?>" placeholder="Is Expendables">
            </div>
            <div class="form-group <?php echo form_error('a_asset_code') ? 'has-error': ''; ?>">
               <div class="col-md-6" style="padding: 0px 0px">
                   <label for="title">Asset Code</label>
                   <input type="text" class="form-control" name="a_asset_code" id="a_asset_code" value="<?php echo set_value('a_asset_code'); ?>" placeholder="Asset Code">
               </div>
                <div class="col-md-6" style="padding: 0px 0px">
                    <label for="title">Serial</label>
                    <input type="text" class="form-control" name="a_model" id="a_model" value="<?php echo set_value('a_model'); ?>" placeholder="Asset Model">
                </div>
            </div>
            <div class="form-group <?php echo form_error('a_supplier_id') ? 'has-error': ''; ?>">
                <label for="title">Supplier</label>
                <?php
                echo form_dropdown('a_supplier_id', $all_supplier, set_value('a_supplier_id'), 'class="form-control"');
                ?>
            </div>
            <div class="form-group <?php echo form_error('a_description') ? 'has-error': ''; ?>">
                <label for="title">Description</label>
                <textarea name="a_description" class="form-control" rows="3"><?php echo set_value('a_description'); ?></textarea>
            </div>
            <div class="form-group <?php echo form_error('a_cost') ? 'has-error': ''; ?>">
                <label for="title">Price</label>
                <input type="text" class="form-control" name="a_cost" id="a_cost" value="<?php echo set_value('a_cost'); ?>" placeholder="Asset Cost">
            </div>
<!--            <div class="form-group --><?php //echo form_error('as_purchase_date') ? 'has-error': ''; ?><!--">-->
<!--                <label for="title">Purchase Date</label>-->
<!--                <input type="text" class="form-control" name="as_purchase_date" id="as_purchase_date" value="--><?php //echo set_value('as_purchase_date'); ?><!--" placeholder="Purchase Date">-->
<!--            </div>-->
            <div class="form-group <?php echo form_error('a_purchase_date') ? 'has-error': ''; ?>">
                <label for="title">Purchase Date</label>
                <div class='input-group date' id='a_purchase_date'>
                    <input type='text' name="a_purchase_date" class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
                <script type="text/javascript">
                    jQuery(function () {
                        jQuery('#a_purchase_date').datetimepicker();
                    });
                </script>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group <?php echo form_error('ai_number_in_stock') ? 'has-error': ''; ?>">
                <label for="title">Number in Stock</label>
                <input type="text" class="form-control" name="ai_number_in_stock" id="ai_number_in_stock" value="<?php echo set_value('ai_number_in_stock'); ?>" placeholder="Number in Stock">
            </div>
            <div class="form-group <?php echo form_error('ai_other_detail') ? 'has-error': ''; ?>">
                <label for="title">Remark</label>
                <input type="text" class="form-control" name="ai_other_detail" id="ai_other_detail" value="<?php echo set_value('ai_other_detail'); ?>" placeholder="Other Detail">
            </div>
            <div class="form-group <?php echo form_error('status') ? 'has-error': ''; ?>">
                <label for="title">Status</label>
                <?php
                echo form_dropdown('status', array('N' => 'New', 'A' => 'Assigned', 'U' => 'Unassigned', 'B' => 'Broken', 'D' => 'Disposed'), set_value('status'), 'class="form-control"');
                ?>
            </div>
        </div>
    </div>

    <a class="btn btn-success" href="<?php echo $baseUrl . "asset/index";?>">Back</a>
    <input type="submit" class="btn btn-primary" name="submit" value="Create" />
<?php echo form_close(); ?>