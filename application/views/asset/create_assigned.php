<?php
$baseUrl = base_url().'index.php/';
?>
<h1><?php echo $title; ?></h1>
<?php if(validation_errors() != false) { ?>
	<div class="alert alert-danger" role="alert">
		<?php echo validation_errors(); ?>	
	</div>
<?php } ?>
<?php echo form_open('asset/create_assigned'); ?>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group <?php echo form_error('ea_employee_id') ? 'has-error': ''; ?>">
                <label for="title">Employee</label>
                <?php
                echo form_dropdown('ea_employee_id', $all_staff, set_value('ea_employee_id'), 'class="form-control"');
                ?>
            </div>
            <div class="form-group <?php echo form_error('ea_asset_id') ? 'has-error': ''; ?>">
                <label for="title">Device</label>
                <?php
                echo form_dropdown('ea_asset_id', $all_asset, set_value('ea_asset_id'), 'class="form-control"');
                ?>
            </div>
            <div class="form-group <?php echo form_error('ea_date_out') ? 'has-error': ''; ?>">
                <label for="title">Date Out</label>
                <div class='input-group date' id='ea_date_out'>
                    <input type='text' name="ea_date_out" class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
                <script type="text/javascript">
                    jQuery(function () {
                        jQuery('#ea_date_out').datetimepicker();
                    });
                </script>
            </div>

            <div class="form-group <?php echo form_error('ea_date_returned') ? 'has-error': ''; ?>">
                <label for="title">Date Returned</label>
                <div class='input-group date' id='ea_date_returned'>
                    <input type='text' class="form-control" name="ea_date_returned" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
                <script type="text/javascript">
                    jQuery(function () {
                        jQuery('#ea_date_returned').datetimepicker();
                    });
                </script>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="title">Condition Out</label>
                <textarea name="ea_condition_out" class="form-control" rows="3"><?php echo set_value('ea_condition_out');?></textarea>
            </div>
            <div class="form-group">
                <label for="title">Condition Returned</label>
                <textarea name="ea_condition_returned" class="form-control" rows="3"><?php echo set_value('ea_condition_returned');?></textarea>
            </div>
            <div class="form-group <?php echo form_error('status') ? 'has-error': ''; ?>">
                <label for="title">Status</label>
                <?php
                echo form_dropdown('status', array('N' => 'New', 'A' => 'Assigned', 'U' => 'Unassigned', 'B' => 'Broken', 'D' => 'Disposed'), set_value('status'), 'class="form-control"');
                ?>
            </div>
        </div>
    </div>
    <a class="btn btn-success" href="<?php echo $baseUrl . "asset/index";?>">Back</a>
    <input type="submit" class="btn btn-primary" name="submit" value="Create" />
<?php echo form_close(); ?>