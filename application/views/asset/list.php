<?php
$baseUrl = base_url() . 'index.php/';
?>

<h1><?php echo $title; ?></h1>
<?php echo form_open('asset/index' , array('class' => 'form-horizontal')); ?>
<div class="input-group" style="margin-bottom: 12px;">
    <div class="input-group-addon">Keyword </div>
    <?php
    echo form_dropdown('type_search', $all_search_field, set_value('type_search'), array('class' => "form-control", "style" => 'width: 7%'));
    ?>
    <input type="text" class="form-control" style="width: 20%" name="search_name" id="search_name" value="<?php echo set_value('search_name'); ?>">
    <input type="submit" name="submit" class="btn btn-primary" value="Search">
</div>
</form>
<?php echo form_open('asset/deleteMultiple' , array('class' => 'form-horizontal')); ?>
<?php if ($this->session->flashdata('asset_success')) { ?>
    <br>
    <div class="alert alert-success alert-dismissible" role="alert" style="margin-top: 20px;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
        <?php echo $this->session->flashdata('asset_success'); ?>
    </div>
<?php } ?>
<?php if ($this->session->flashdata('asset_add')) { ?>
    <br>
    <div class="alert alert-success alert-dismissible" role="alert" style="margin-top: 20px;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
        <?php echo $this->session->flashdata('asset_add'); ?>
    </div>
<?php } ?>
<?php if ($this->session->flashdata('asset_delete')) { ?>
    <br>
    <div class="alert alert-success alert-dismissible" role="alert" style="margin-top: 20px;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
        <?php echo $this->session->flashdata('asset_delete'); ?>
    </div>
<?php } ?>
<input type="submit" name="submit" class="btn btn-danger" value="Delete Selected Items">
<a class="btn btn-success pull-right" href="<?php echo $baseUrl . "asset/create"; ?>">Create</a>
<hr>
<div id="form_input">
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th style="width: 4%"><a href="<?php echo $baseUrl; ?>asset/index/<?= $page ?>/a_seq/<?= $order ?>"><input type="checkbox" id="checkAll"><i
                            class="fa fa-fw fa-sort"></i>ID</a></th>
                <th style="width: 9%"><a href="<?php echo $baseUrl; ?>asset/index/<?= $page ?>/a_name/<?= $order ?>"><i
                            class="fa fa-fw fa-sort"></i>Name</a></th>
                <th>Category</th>
                <th>Is Expendables</th>
                <th>Description</th>
                <th>Assigned User</th>
                <th><a href="<?php echo $baseUrl; ?>asset/index/<?= $page ?>/assets.wdate/<?= $order ?>"><i
                            class="fa fa-fw fa-sort"></i>R.Date</a></th>
<!--                <th>In Stock</th>-->
<!--                <th>Assigned</th>-->
                <th>Status</th>
                <th style="width: 15%">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php if (!empty($results)) : ?>
                <?php foreach ($results as $data) { ?>
                    <tr>
                        <td><input type="checkbox" name="asset_id[]" value="<?php echo $data->a_seq; ?>"><a href=""><?php echo $data->a_seq; ?></a></td>
                        <td><?php echo $data->a_name; ?></td>
                        <td style="width: 6%"><?php echo $this->Asset_Category_Model->get_asset_category_name($data->a_category); ?></td>
                        <td style="width: 6%"><?php echo $data->a_is_expendables; ?></td>
                        <td style="width: 10%"><?php echo $data->a_description; ?></td>
                        <td style="width: 7%"><?php echo $this->Employee_Asset_Model->getAssignedUser($data->a_seq);?></td>
                        <td style="width: 7%"><?php echo (isset($data->a_wdate) && $data->a_wdate != 0) ? date('Y-m-d', $data->a_wdate) : ''; ?></td>
<!--                        <td style="width: 5%">--><?php //echo $data->ai_number_in_stock; ?><!--</td>-->
<!--                        <td style="width: 2%">--><?php //echo $this->Employee_Asset_Model->record_count_by_asset($data->a_seq);?><!--</td>-->
                        <td style="width: 2%"><?php echo $this->Common_Model->getStatusAsset($data->a_status);?></td>
                        <td class="text-left" style="width: 15%">
                            <a class="btn btn-default" href="<?php echo $baseUrl . "asset/view/" . $data->a_seq; ?>">View</a>
                            <a class="btn btn-success" data-toggle="modal" data-target="#modalList" onclick="add_person(<?php echo $data->a_seq;?>, null)">Assign</a>
                            <a class="btn btn-warning" data-toggle="modal" data-target="#modalUnassign" onclick="edit_person(<?php echo $data->a_seq;?>)">Un-Assign</a>
                            <a class="btn btn-primary" href="<?php echo $baseUrl . "asset/update/" . $data->a_seq; ?>">Update</a>
                            <a class="btn btn-danger" href="<?php echo $baseUrl . "asset/delete/" . $data->a_seq; ?>">Delete</a>
                        </td>
                    </tr>
                <?php } ?>
            <?php endif; ?>
            </tbody>
        </table>
        </div>
</div>
</form>
<div class="text-center">
    <p><?php echo $links; ?></p>
</div>
        <!-- Modal -->
        <div class="modal fade" id="modalList" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Assigning Asset</h4$>
                    </div>
                    <div class="modal-body assign_modal">
                        <h4><b>Assets Name</b></h4>
                        <div id="asset_name"></div>
                        <h4><b>Category Code</b></h4>
                        <div id="asset_category_code"></div>
                        <div class="row">
                                <div class="panel panel-default" style="margin: 0 12px">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Assigning User</h3>
                                    </div>
                                    <div class="panel-body">
                                            <div class="form-group">
                                                <label for="exampleInputName2">Department</label>
                                                <select name="department_id" id="department_id" class="form-control">
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail2">Employee</label>
                                                <select class="form-control" name="employee_id" id="employee_id">
                                                </select>
                                                <input type="hidden" name="asset_id" id="asset_id" value="">
                                            </div>
                                            <div class="alert alert-danger hide">
                                            </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <input type="submit" id="btnSave" class="btn btn-primary" onclick="save()" value="Save">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalUnassign" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Assigning Asset</h4$>
                    </div>
                    <div class="modal-body unassign_modal">
                        <h4><b>Assets Name</b></h4>
                        <div id="asset_name_unassign"></div>
                        <h4><b>Category Code</b></h4>
                        <div id="asset_category_code_unassign"></div>
                        <div class="row">
                            <div class="panel panel-default" style="margin: 0 12px">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Assigning User</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="exampleInputName2">Department</label>
                                        <div id="department_unassign"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail2">Employee</label>
                                        <div id="employee_unassign"></div>
                                        <input type="hidden" name="asset_id_unassign" id="asset_id_unassign" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body unassign_modal_error hide"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <input type="submit" id="btnUnassigned" class="btn btn-primary" onclick="unassigned()" value="Unassigned">
                    </div>
                </div>
            </div>
        </div>
<script type="text/javascript">
    $("#checkAll").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });
</script>