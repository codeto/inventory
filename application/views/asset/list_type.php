<?php
$baseUrl = base_url() . 'index.php/';
?>
<h1><?php echo $title; ?></h1>

    <?php echo form_open('asset/check' , array('class' => 'form-horizontal')); ?>
        <label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
        <div class="input-group" style="margin-bottom: 12px;">
            <div class="input-group-addon">Keyword </div>
            <select class="form-control" style="width: 5%">
                <option>Name</option>
                <option>Code</option>
                <option>Description</option>
            </select>
            <input type="text" class="form-control" style="width: 20%" id="exampleInputAmount">

            <button type="button" class="btn btn-primary">
                <span class="glyphicon glyphicon-search"></span>Search
            </button>
        </div>
    <input type="submit" name="submit" class="btn btn-danger" value="Delete Selected Items">
<?php if ($this->session->flashdata('type_success')) { ?>
    <br>
    <div class="alert alert-success alert-dismissible" role="alert" style="margin-top: 20px;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
        <?php echo $this->session->flashdata('type_success'); ?>
    </div>
<?php } ?>
<?php if ($this->session->flashdata('type_add')) { ?>
    <br>
    <div class="alert alert-success alert-dismissible" role="alert" style="margin-top: 20px;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
        <?php echo $this->session->flashdata('type_add'); ?>
    </div>
<?php } ?>
<?php if ($this->session->flashdata('type_delete')) { ?>
    <br>
    <div class="alert alert-success alert-dismissible" role="alert" style="margin-top: 20px;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
        <?php echo $this->session->flashdata('type_delete'); ?>
    </div>
<?php } ?>
<a class="btn btn-success pull-right" href="<?php echo $baseUrl . "asset/create_type"; ?>">Create</a>
<hr>
<div id="form_input">
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th style="width: 4%"><a href="<?php echo $baseUrl; ?>asset/type/<?= $page ?>/at_seq/<?= $order ?>"><input type="checkbox" id="checkAll">ID</a><i
                        class="fa fa-fw fa-sort"></i></th>
                <th style="width: 9%"><a href="<?php echo $baseUrl; ?>asset/type/<?= $page ?>/at_name/<?= $order ?>"><i
                            class="fa fa-fw fa-sort"></i>Name</a></th>
                <th>Description</th>
                <th>Count</th>
                <th style="width: 9%"><a href="<?php echo $baseUrl; ?>asset/type/<?= $page ?>/wdate/<?= $order ?>"><i
                            class="fa fa-fw fa-sort"></i>Write Date</a></th>
                <th style="width: 9%"><a href="<?php echo $baseUrl; ?>asset/type/<?= $page ?>/mdate/<?= $order ?>"><i
                            class="fa fa-fw fa-sort"></i>Modify Date</a></th>
                <th>Status</th>
                <th style="width: 15%">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php if (!empty($results)) : ?>
                <?php foreach ($results as $data) { ?>
                    <tr>
                        <td><input type="checkbox" name="ca_name[]" value="<?php echo $data->at_seq; ?>"><a href="#"><?php echo $data->at_seq; ?></a></td>
                        <td><?php echo $data->at_name; ?></td>
                        <td><?php echo $data->at_description; ?></td>
                        <td>1222</td>
                        <td><?php echo date('Y:m:d h:i:s', $data->wdate); ?></td>
                        <td><?php echo (isset($data->mdate) && $data->mdate != 0) ? date('Y:m:d h:i:s', $data->mdate) : ''; ?></td>
                        <!--						<td>-->
                        <?php //echo $this->common_model->getStatus($data->status);?><!--</td>-->
                        <td class="text-center" style="width: 5%">Y</td>
                        <td class="text-left">
                            <a class="btn btn-default"
                               href="<?php echo $baseUrl . "asset/view_type/" . $data->at_seq; ?>">View</a>
                            <a class="btn btn-primary"
                               href="<?php echo $baseUrl . "asset/update_type/" . $data->at_seq; ?>">Update</a>
                            <a class="btn btn-danger"
                               href="<?php echo $baseUrl . "asset/delete_type/" . $data->at_seq; ?>">Delete</a>
                        </td>
                    </tr>
                <?php } ?>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>
</form>
<div class="text-center">
    <p><?php echo $links; ?></p>
</div>
<script type="text/javascript">
    $("#checkAll").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });
</script>