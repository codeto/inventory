<?php
$baseUrl = base_url() . 'index.php/';
?>
<h1><?php echo $title; ?></h1>
<hr>
<div id="form_input">
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th style="width: 4%"><a
                        href="<?php echo base_url(); ?>asset/member/<?= $page ?>/ea_seq/<?= $order ?>"><i
                            class="fa fa-fw fa-sort"></i>ID</a></th>
                <th>Asset Name</th>
                <th>Employee</th>
                <th>Date Out</th>
                <th>Date Returned</th>
                <th>Condition Out</th>
                <th>Condition Returned</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <?php if (!empty($results)) : ?>
                <?php foreach ($results as $data) { ?>
                    <tr>
                        <td><a href=""><?php echo $data->ea_seq; ?></a></td>
                        <td><?php echo $this->Asset_Model->get_asset_name($data->ea_asset_id); ?></td>
                        <td><?php echo $this->Employee_Model->get_employee_name($data->ea_employee_id); ?></td>
                        <td><?php echo date('Y:m:d h:i:s', $data->ea_date_out); ?></td>
                        <td><?php echo (isset($data->ea_date_returned) && $data->ea_date_returned != 0) ? date('Y:m:d h:i:s', $data->ea_date_returned) : ''; ?></td>
                        <td><?php echo $data->ea_condition_out; ?></td>
                        <td><?php echo $data->ea_condition_returned; ?></td>
                        <td class="text-center"><span class="glyphicon glyphicon-ok-circle"></span></td>
                    </tr>
                <?php } ?>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>
<div class="text-center">
    <p><?php echo $links; ?></p>
</div>