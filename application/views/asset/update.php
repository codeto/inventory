<?php
$baseUrl = base_url().'index.php/';
?>
<h1><?php echo $title; ?></h1>
<?php if(validation_errors() != false) { ?>
	<div class="alert alert-danger" role="alert">
		<?php echo validation_errors(); ?>
	</div>
<?php } ?>
<?php echo form_open('asset/update/'.$asset_item->a_seq) ?>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group <?php echo form_error('a_name') ? 'has-error': ''; ?>">
                <label for="title">Name</label>
                <input type="text" class="form-control" name="a_name" id="a_name" value="<?php echo set_value('a_name') == false ? $asset_item->a_name : set_value('a_name');?>">
            </div>
            <div class="form-group <?php echo form_error('as_asset_type_id') ? 'has-error': ''; ?>">
                <label for="title">Category</label>
                <?php
                echo form_dropdown('a_category', $all_asset_type, set_value('a_category',$asset_item->a_category ), 'class="form-control"');
                ?>
            </div>
            <div class="form-group <?php echo form_error('a_is_expendables') ? 'has-error': ''; ?>">
                <label for="title">Is Expendables</label>
                <input type="text" class="form-control" name="a_is_expendables" id="a_is_expendables" value="<?php echo set_value('a_is_expendables') == false ? $asset_item->a_is_expendables : set_value('a_is_expendables');?>">
            </div>
            <div class="form-group <?php echo form_error('a_asset_code') ? 'has-error': ''; ?>">
                <label for="title">Asset Code</label>
                <input type="text" class="form-control" name="a_asset_code" id="a_asset_code" value="<?php echo set_value('a_asset_code') == false ? $asset_item->a_asset_code : set_value('a_asset_code');?>">
            </div>

            <div class="form-group <?php echo form_error('a_supplier_id') ? 'has-error': ''; ?>">
                <label for="title">Supplier</label>
                <?php
                echo form_dropdown('a_supplier_id', $all_supplier, set_value('a_supplier_id', $asset_item->a_supplier_id), 'class="form-control"');
                ?>
            </div>
            <div class="form-group <?php echo form_error('a_description') ? 'has-error': ''; ?>">
                <label for="title">Description</label>
                <textarea name="a_description" class="form-control" rows="3"><?php echo set_value('a_description', $asset_item->a_description);?></textarea>
            </div>
            <div class="form-group <?php echo form_error('a_cost') ? 'has-error': ''; ?>">
                <label for="title">Price</label>
                <input type="text" class="form-control" name="a_cost" id="a_cost" value="<?php echo set_value('a_cost', $asset_item->a_cost);?>">
            </div>
            <div class="form-group <?php echo form_error('a_model') ? 'has-error': ''; ?>">
                <label for="title">Serial</label>
                <input type="text" class="form-control" name="a_model" id="a_model" value="<?php echo set_value('a_model', $asset_item->a_model);?>">
            </div>
            <div class="form-group <?php echo form_error('a_purchase_date') ? 'has-error': ''; ?>">
                <label for="title">Purchase Date</label>
                <div class='input-group date' id='a_purchase_date'>
                    <input type='text' name="a_purchase_date" class="form-control" value="<?php echo set_value('a_purchase_date', date('m/d/Y h:i A',$asset_item->a_purchase_date));?>"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                </div>
                <script type="text/javascript">
                    jQuery(function () {
                        jQuery('#a_purchase_date').datetimepicker();
                    });
                </script>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="title">Number in Stock</label>
                <input type="text" class="form-control" name="ai_number_in_stock" id="ai_number_in_stock" value="<?php echo set_value('ai_number_in_stock', isset($asset_inventory_item) ? $asset_inventory_item->ai_number_in_stock : '');?>" placeholder="Number in Stock">
            </div>
            <div class="form-group">
                <label for="title">Remark</label>
                <input type="text" class="form-control" name="ai_other_detail" id="ai_other_detail" value="<?php echo set_value('ai_other_detail', isset($asset_inventory_item) ? $asset_inventory_item->ai_other_detail : '');?>" placeholder="Other Detail">
            </div>
            <div class="form-group <?php echo form_error('status') ? 'has-error': ''; ?>">
                <label for="title">Status</label>
                <?php
                echo form_dropdown('status', array('N' => 'New', 'A' => 'Assigned', 'U' => 'Unassigned', 'B' => 'Broken', 'D' => 'Disposed'), set_value('status', $asset_item->status) , 'class="form-control"');
                ?>
            </div>
        </div>
    </div>
	<a class="btn btn-success" href="<?php echo $baseUrl . "asset/index";?>">Back</a>
    <input type="submit" class="btn btn-primary" name="submit" value="Update" />
<?php echo form_close(); ?>