<h1><?php echo $title; ?></h1>
<?php if(validation_errors() != false) { ?>
	<div class="alert alert-danger" role="alert">
		<?php echo validation_errors(); ?>	
	</div>
<?php } ?>
<?php echo form_open('asset/update_type/'.$asset_type_item->at_seq) ?>
	<div class="form-group <?php echo form_error('at_name') ? 'has-error': ''; ?>">
	    <label for="title">Type Name</label>
    	<input type="text" class="form-control" name="at_name" id="at_name" value="<?php echo set_value('at_name', $asset_type_item->at_name);?>">
	</div>
	<div class="form-group <?php echo form_error('at_description') ? 'has-error': ''; ?>">
		<label for="title">Type Description</label>
		<textarea name="at_description" class="form-control" rows="3"><?php echo set_value('at_description', $asset_type_item->at_description);?></textarea>
	</div>
	<a class="btn btn-success" href="<?php echo base_url() . "/asset/type";?>">Back</a>
    <input type="submit" class="btn btn-primary" name="submit" value="Update" />
<?php echo form_close(); ?>