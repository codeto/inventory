<?php
$baseUrl = base_url().'index.php/';
?>
    <h1><?php echo $title;?></h1>
    <div class="well">
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <td style="width: 20%">Asset Name</td>
                    <td><?php echo $asset_info->a_name;?></td>
                </tr>
                <tr>
                    <td>Asset Description</td>
                    <td><?php echo $asset_info->a_description;?></td>
                </tr>
                <tr>
                    <td>Created At</td>
                    <td><?php echo date('Y:m:d h:i:s',$asset_info->wdate);?></td>
                </tr>
                <tr>
                    <td>Update At</td>
                    <td><?php echo date('Y:m:d h:i:s',$asset_info->mdate);?></td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td><?php echo $this->Common_Model->getStatusAsset($asset_info->status);?></td>
                </tr>
            </table>
        </div>
        <div class="row">
            <a class="btn btn-success" href="<?php echo $baseUrl . "asset/index";?>">Back</a>
        </div>
    </div>