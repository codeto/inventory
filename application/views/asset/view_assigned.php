<?php
$baseUrl = base_url().'index.php/';
?>
    <h1><?php echo $title;?></h1>
    <div class="well">
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <td style="width: 20%">Asset Name</td>
                    <td><?php echo $employee_asset_info->ea_employee_id;?></td>
                </tr>
                <tr>
                    <td>Asset Description</td>
                    <td><?php echo $employee_asset_info->ea_asset_id;?></td>
                </tr>
                <tr>
                    <td>Date Out</td>
                    <td><?php echo date('Y:m:d h:i:s',$employee_asset_info->ea_date_out);?></td>
                </tr>
                <tr>
                    <td>Date Returned</td>
                    <td><?php echo date('Y:m:d h:i:s',$employee_asset_info->ea_date_returned);?></td>
                </tr>
                <tr>
                    <td>Other Detail</td>
                    <td><?php echo $employee_asset_info->ea_other_detail;?></td>
                </tr>
                <tr>
                    <td>Condition Out</td>
                    <td><?php echo $employee_asset_info->ea_condition_out;?></td>
                </tr>
                <tr>
                    <td>Condition Returned</td>
                    <td><?php echo $employee_asset_info->ea_condition_returned;?></td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td><?php echo $this->Common_Model->getStatus($employee_asset_info->status);?></td>
                </tr>
            </table>
        </div>
        <div class="row">
            <a class="btn btn-success" href="<?php echo $baseUrl . "/asset/assigned";?>">Back</a>
        </div>
    </div>