<?php
$baseUrl = base_url().'index.php/';
?>
    <h1><?php echo $title;?></h1>
    <div class="well">
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <td style="width: 20%">Type Name</td>
                    <td><?php echo $type_info->at_name;?></td>
                </tr>
                <tr>
                    <td>Type Description</td>
                    <td><?php echo $type_info->at_description;?></td>
                </tr>
                <tr>
                    <td>Created At</td>
                    <td><?php echo date('Y:m:d h:i:s',$type_info->wdate);?></td>
                </tr>
                <tr>
                    <td>Update At</td>
                    <td><?php echo date('Y:m:d h:i:s',$type_info->mdate);?></td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td><?php echo $this->Common_Model->getStatus($type_info->status);?></td>
                </tr>
            </table>
        </div>
        <div class="row">
            <a class="btn btn-success" href="<?php echo $baseUrl . "asset/type";?>">Back</a>
        </div>
    </div>