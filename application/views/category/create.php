<?php
$baseUrl = base_url().'index.php/';
?>
<h1><?php echo $title; ?></h1>
<?php if(validation_errors() != false) { ?>
	<div class="alert alert-danger" role="alert">
		<?php echo validation_errors(); ?>	
	</div>
<?php } ?>
<?php echo form_open('category/create'); ?>
	<div class="form-group <?php echo form_error('ac_name') ? 'has-error': ''; ?>">
	    <label for="title">Name</label>
    	<input type="text" class="form-control" name="ac_name" id="at_name" value="<?php echo set_value('ac_name'); ?>" placeholder="Category Name">
	</div>
	<div class="form-group <?php echo form_error('ac_code') ? 'has-error': ''; ?>">
	    <label for="title">Name</label>
    	<input type="text" class="form-control" name="ac_code" id="ac_code" value="<?php echo set_value('ac_code'); ?>" placeholder="Category Code">
	</div>
	<div class="form-group <?php echo form_error('ac_remark') ? 'has-error': ''; ?>">
		<label for="title">Remark</label>
		<textarea name="ac_remark" class="form-control" rows="3"><?php echo set_value('ac_remark'); ?></textarea>
	</div>
	<div class="form-group <?php echo form_error('status') ? 'has-error': ''; ?>">
		<label for="title">Status</label>
		<?php
		echo form_dropdown('status', array(0 => 'N', 1 => 'Y'), set_value('status'), 'class="form-control"');
		?>
	</div>
	<a class="btn btn-success" href="<?php echo $baseUrl . "asset/type";?>">Back</a>
    <input type="submit" class="btn btn-primary" name="submit" value="Create" />
<?php echo form_close(); ?>