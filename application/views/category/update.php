<h1><?php echo $title; ?></h1>
<?php if(validation_errors() != false) { ?>
	<div class="alert alert-danger" role="alert">
		<?php echo validation_errors(); ?>	
	</div>
<?php } ?>
<?php echo form_open('category/update/'.$asset_type_item->ac_seq) ?>
<div class="col-md-6">
	<div class="form-group <?php echo form_error('ac_name') ? 'has-error': ''; ?>">
	    <label for="title">Type Name</label>
    	<input type="text" class="form-control" name="ac_name" id="ac_name" value="<?php echo set_value('ac_name', $asset_type_item->ac_name);?>">
	</div>
	<div class="form-group <?php echo form_error('ac_code') ? 'has-error': ''; ?>">
	    <label for="title">Type Name</label>
    	<input type="text" class="form-control" name="ac_code" id="ac_code" value="<?php echo set_value('ac_code', $asset_type_item->ac_code);?>">
	</div>
	<div class="form-group <?php echo form_error('ac_remark') ? 'has-error': ''; ?>">
		<label for="title">Remark</label>
		<textarea name="ac_remark" class="form-control" rows="3"><?php echo set_value('ac_remark', $asset_type_item->ac_remark);?></textarea>
	</div>
	<div class="form-group <?php echo form_error('status') ? 'has-error': ''; ?>">
		<label for="title">Status</label>
		<?php
		echo form_dropdown('status', array(0 => 'N', 1 => 'Y'), set_value('status', $asset_type_item->status) , 'class="form-control"');
		?>
	</div>
	<a class="btn btn-success" href="<?php echo base_url() . "/asset/type";?>">Back</a>
    <input type="submit" class="btn btn-primary" name="submit" value="Update" />
</div>
<?php echo form_close(); ?>