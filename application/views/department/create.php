<?php
$baseUrl = base_url().'index.php/';
?>
<h1><?php echo $title; ?></h1>
<?php if(validation_errors() != false) { ?>
	<div class="alert alert-danger" role="alert">
		<?php echo validation_errors(); ?>	
	</div>
<?php } ?>
<?php echo form_open('department/create'); ?>
	<div class="form-group <?php echo form_error('de_name') ? 'has-error': ''; ?>">
	    <label for="title">Name</label>
    	<input type="text" class="form-control" name="de_name" id="de_name" value="<?php echo set_value('de_name'); ?>" placeholder="Department Name">
	</div>
	<div class="form-group <?php echo form_error('de_description') ? 'has-error': ''; ?>">
		<label for="title">Description</label>
		<textarea name="de_description" value="<?php echo set_value('de_description'); ?>" class="form-control" rows="3"></textarea>
	</div>
	<div class="form-group">
		<label>
			<input type="radio" name="status" value="1" <?php echo (set_value('status') == '1') ? 'checked' : ''; ?>>
			Active
			<input type="radio" name="status" value="0" <?php echo (set_value('status') == '0') ? 'checked' : ''; ?>>
			Deactive
		</label>
	</div>
	<a class="btn btn-success" href="<?php echo $baseUrl . "department/index";?>">Back</a>
    <input type="submit" class="btn btn-primary" name="submit" value="Create" />
<?php echo form_close(); ?>