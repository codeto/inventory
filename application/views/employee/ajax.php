<?php
$baseUrl = base_url().'index.php/';
?>
	<h1>List Employee</h1>
	<a class="btn btn-success" href="<?php echo $baseUrl . "employee/create";?>">Create New Employee</a>
    <?php if ($this->session->flashdata('employee_success')) { ?>
        <br>
        <div class="alert alert-success alert-dismissible" role="alert" style="margin-top: 20px;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('employee_success'); ?>
        </div>
    <?php } ?>
    <?php if ($this->session->flashdata('employee_add')) { ?>
        <br>
        <div class="alert alert-success alert-dismissible" role="alert" style="margin-top: 20px;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('employee_add'); ?>
        </div>
    <?php } ?>
    <?php if ($this->session->flashdata('employee_delete')) { ?>
        <br>
        <div class="alert alert-success alert-dismissible" role="alert" style="margin-top: 20px;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('employee_delete'); ?>
        </div>
    <?php } ?>
    <hr>
	<div id="form_input">
		<div class="table-responsive">
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th style="width: 4%"><a href="<?php echo $baseUrl;?>employee/index/<?=$page?>/em_seq/<?=$order?>"><i class="fa fa-fw fa-sort"></i>ID</a></th>
					<th style="width: 9%"><a href="<?php echo $baseUrl;?>employee/index/<?=$page?>/em_first_name/<?=$order?>"><i class="fa fa-fw fa-sort"></i>Firstname</a></th>
					<th>Lastname</th>
					<th>Department</th>
					<th>CellPhone</th>
					<th><a href="<?php echo $baseUrl;?>employee/index/<?=$page?>/em_email_address/<?=$order?>"><i class="fa fa-fw fa-sort"></i>Email</a></th>
					<th>Other</th>
					<th>Status</th>
					<th style="width: 15%">Action</th>
				</tr>
					</thead>
			<tbody>
				<?php if(!empty($results)) : ?>
					<?php foreach ($results as $data) { ?>
						<tr>
							<td><a href=""><?php echo $data->em_seq;?></a></td>
							<td><?php echo $data->em_first_name;?></td>
							<td><?php echo $data->em_last_name;?></td>
							<td><?php echo $this->Department_Model->get_department_name($data->em_department_id);?></td>
							<td><?php echo $data->em_cell_mobile;?></td>
							<td><?php echo $data->em_email_address;?></td>
							<td><?php echo $data->em_other_detail;?></td>
							<td class="text-center"><span class="glyphicon glyphicon-ok-circle"></span></td>
<!--							<td class="text-center"><span class="glyphicon glyphicon-remove-circle"></span></td>-->
<!--							<td><img src="--><?php //echo base_url(); ?><!--assets/images/active.png" alt="active"></td>-->
<!--							<td>--><?php //echo $this->common_model->getStatus($data->status);?><!--</td>-->
							<td class="text-left">
								<a class="btn btn-default" href="<?php echo $baseUrl . "employee/view/".$data->em_seq; ?>">View</a>
								<a class="btn btn-primary" href="<?php echo $baseUrl . "employee/update/".$data->em_seq; ?>">Update</a>
								<a class="btn btn-danger" href="<?php echo $baseUrl . "employee/delete/".$data->em_seq; ?>">Delete</a>
							</td>
						</tr>
					<?php } ?>
				<?php endif;?>
			</tbody>
		</table>
		</div>
	</div>
	<div id="pagination">
		<p><?php echo $links; ?></p>
	</div>	