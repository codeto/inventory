<?php
$baseUrl = base_url().'index.php/';
?>
<h1><?php echo $title; ?></h1>
<?php if(validation_errors() != false) { ?>
	<div class="alert alert-danger" role="alert">
		<?php echo validation_errors(); ?>	
	</div>
<?php } ?>
<?php echo form_open('employee/create'); ?>
	<div class="form-group <?php echo form_error('em_first_name') ? 'has-error': ''; ?>">
	    <label for="title">Firstname</label>
    	<input type="text" class="form-control" name="em_first_name" id="em_first_name" value="<?php echo set_value('em_first_name'); ?>" placeholder="Firstname">
	</div>
	<div class="form-group <?php echo form_error('em_last_name') ? 'has-error': ''; ?>">
		<label for="title">Lastname</label>
		<input type="text" class="form-control" name="em_last_name" id="em_last_name" value="<?php echo set_value('em_last_name'); ?>" placeholder="Lastname">
	</div>
	<div class="form-group <?php echo form_error('em_department_id') ? 'has-error': ''; ?>">
		<label for="title">Department</label>
		<?php
		echo form_dropdown('em_department_id', $all_department, set_value('em_department_id'), 'class="form-control"');
		?>
	</div>
	<div class="form-group <?php echo form_error('em_cell_mobile') ? 'has-error': ''; ?>">
		<label for="title">Cellphone</label>
		<input type="text" class="form-control" name="em_cell_mobile" id="em_cell_mobile" value="<?php echo set_value('em_cell_mobile'); ?>" placeholder="Cellphone">
	</div>
	<div class="form-group <?php echo form_error('em_email_address') ? 'has-error': ''; ?>">
		<label for="title">Email</label>
		<input type="text" class="form-control" name="em_email_address" id="em_email_address" value="<?php echo set_value('em_email_address'); ?>" placeholder="Email">
	</div>
	<div class="form-group <?php echo form_error('em_other_detail') ? 'has-error': ''; ?>">
		<label for="title">Other Detail</label>
		<textarea name="em_other_detail" class="form-control" rows="3"><?php echo set_value('em_other_detail'); ?></textarea>
	</div>
	<div class="form-group <?php echo form_error('status') ? 'has-error': ''; ?>">
		<label>
			<input type="radio" name="status" value="1" <?php echo (set_value('status') == '1') ? 'checked' : ''; ?>>
			Active
			<input type="radio" name="status" value="0" <?php echo (set_value('status') == '0') ? 'checked' : ''; ?>>
			Deactive
		</label>
	</div>
    <input type="submit" class="btn btn-primary" name="submit" value="Create" />
<?php echo form_close(); ?>