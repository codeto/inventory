<?php
$baseUrl = base_url().'index.php/';
?>
<h1><?php echo $title; ?></h1>
<?php if(validation_errors() != false) { ?>
	<div class="alert alert-danger" role="alert">
		<?php echo validation_errors(); ?>	
	</div>
<?php } ?>
<?php echo form_open('employee/update/'.$employee_item->em_seq) ?>
	<div class="form-group <?php echo form_error('em_first_name') ? 'has-error': ''; ?>">
	    <label for="title">First Name</label>
    	<input type="text" class="form-control" name="em_first_name" id="em_first_name" value="<?php echo set_value('em_first_name',$employee_item->em_first_name);?>" placeholder="Name">
	</div>
	<div class="form-group <?php echo form_error('em_last_name') ? 'has-error': ''; ?>">
		<label for="title">Last Name</label>
		<textarea name="em_last_name" class="form-control" rows="3"><?php echo set_value('em_last_name',$employee_item->em_last_name);?></textarea>
	</div>
	<div class="form-group <?php echo form_error('em_department_id') ? 'has-error': ''; ?>">
		<label for="title">Department</label>
        <?php
        echo form_dropdown('em_department_id', $all_department, set_value('em_department_id', $employee_item->em_department_id) , 'class="form-control"');
        ?>
	</div>
	<div class="form-group <?php echo form_error('em_cell_mobile') ? 'has-error': ''; ?>">
		<label for="title">Cellphone</label>
		<textarea name="em_cell_mobile" class="form-control" rows="3"><?php echo set_value('em_cell_mobile', $employee_item->em_cell_mobile);?></textarea>
	</div>
	<div class="form-group <?php echo form_error('em_email_address') ? 'has-error': ''; ?>">
		<label for="title">Email</label>
		<textarea name="em_email_address" class="form-control" rows="3"><?php echo set_value('em_email_address', $employee_item->em_email_address);?></textarea>
	</div>
	<div class="form-group <?php echo form_error('em_other_detail') ? 'has-error': ''; ?>">
		<label for="title">Other Detail</label>
		<textarea name="em_other_detail" class="form-control" rows="3"><?php echo set_value('em_other_detail', $employee_item->em_other_detail);?></textarea>
	</div>
	<div class="form-group">
		<label>
			<input type="radio" name="status" value="1" <?php echo (set_value('status', $department_item->status) == '1') ? 'checked' : ''; ?>>
			Active
			<input type="radio" name="status" value="0" <?php echo (set_value('status', $department_item->status) == '0') ? 'checked' : ''; ?>>
			Deactive
		</label>
	</div>
	<a class="btn btn-success" href="<?php echo $baseUrl . "employee/index";?>">Back</a>
    <input type="submit" class="btn btn-primary" name="submit" value="Update" />
<?php echo form_close(); ?>