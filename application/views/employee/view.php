<?php
$baseUrl = base_url().'index.php/';
?>
    <h1><?php echo $title;?></h1>
    <div class="well">
        <div class="row">
            <a class="btn btn-success" style="margin-left: 15px;margin-bottom: 12px;" href="<?php echo $baseUrl . "employee/index";?>">Back</a>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <td style="width: 20%">First Name</td>
                    <td><?php echo $employee_info->em_first_name;?></td>
                </tr>
                <tr>
                    <td>Last Name</td>
                    <td><?php echo $employee_info->em_last_name;?></td>
                </tr>
                <tr>
                    <td>Department</td>
                    <td><?php echo $department_name;?></td>
                </tr>
                <tr>
                    <td>Cellphone</td>
                    <td><?php echo $employee_info->em_cell_mobile;?></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><?php echo $employee_info->em_email_address;?></td>
                </tr>
                <tr>
                    <td>Other detail</td>
                    <td><?php echo $employee_info->em_other_detail;?></td>
                </tr>
                <tr>
                    <td>Created At</td>
                    <td><?php echo date('Y:m:d h:i:s',$employee_info->wdate);?></td>
                </tr>
                <tr>
                    <td>Update At</td>
                    <td><?php echo date('Y:m:d h:i:s',$employee_info->mdate);?></td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td><?php echo $this->Common_Model->getStatus($employee_info->status);?></td>
                </tr>
            </table>
        </div>

        <?php if(!empty($results)) : ?>
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th style="width: 4%"><a href="#"><i class="fa fa-fw fa-sort"></i>ID</a></th>
                        <th>Asset Name</th>
                        <th>Employee</th>
                        <th>Date Out</th>
                        <th>Date Returned</th>
                        <th>Condition Out</th>
                        <th>Condition Returned</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($results as $data) { ?>
                        <tr>
                            <td><a href=""><?php echo $data->ea_seq;?></a></td>
                            <td><?php echo $this->Asset_Model->get_asset_name($data->ea_asset_id);?></td>
                            <td><?php echo $this->Employee_Model->get_employee_name($data->ea_employee_id);?></td>
                            <td><?php echo date('Y:m:d h:i:s',$data->ea_date_out);?></td>
                            <td><?php echo (isset($data->ea_date_returned) && $data->ea_date_returned !=0) ? date('Y:m:d h:i:s',$data->ea_date_returned) : '';?></td>
                            <td><?php echo $data->ea_condition_out ;?></td>
                            <td><?php echo $data->ea_condition_returned ;?></td>
                            <td class="text-center" style="width: 5%"><?php echo $this->Common_Model->getStatus($data->status);?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <div id="pagination">
                <p><?php echo $links; ?></p>
            </div>
        <?php endif;?>
    </div>