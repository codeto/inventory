<?php
$baseUrl = base_url() . 'index.php/';
?>
<h1><?php echo $title; ?></h1>
        <?php echo form_open('setting/categories' , array('class' => 'form-horizontal')); ?>
        <div class="input-group" style="margin-bottom: 12px;">
            <div class="input-group-addon">Keyword </div>

            <?php
            echo form_dropdown('type_search', $all_search_field, set_value('type_search'), array('class' => "form-control", "style" => 'width: 7%'));
            ?>
            <input type="text" class="form-control" style="width: 20%" name="search_name" id="search_name" value="<?php echo set_value('search_name'); ?>">
            <input type="submit" name="submit" class="btn btn-primary" value="Search">
        </div>
        </form>
<?php if ($this->session->flashdata('type_success')) { ?>
    <br>
    <div class="alert alert-success alert-dismissible" role="alert" style="margin-top: 20px;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
        <?php echo $this->session->flashdata('type_success'); ?>
    </div>
<?php } ?>
<?php if ($this->session->flashdata('type_add')) { ?>
    <br>
    <div class="alert alert-success alert-dismissible" role="alert" style="margin-top: 20px;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
        <?php echo $this->session->flashdata('type_add'); ?>
    </div>
<?php } ?>
<?php if ($this->session->flashdata('type_delete')) { ?>
    <br>
    <div class="alert alert-success alert-dismissible" role="alert" style="margin-top: 20px;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
        <?php echo $this->session->flashdata('type_delete'); ?>
    </div>
<?php } ?>
<?php echo form_open('category/deleteMultiple' , array('class' => 'form-horizontal')); ?>
    <input type="submit" name="submit" class="btn btn-danger" value="Delete Selected Items">
<a class="btn btn-success pull-right" href="<?php echo $baseUrl . "category/create"; ?>">Create</a>
<hr>
<div id="form_input">
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th style="width: 4%"><a href="<?php echo $baseUrl; ?>setting/categories/<?= $page ?>/ac_seq/<?= $order ?>"><input type="checkbox" id="checkAll">ID</a><i
                        class="fa fa-fw fa-sort"></i></th>
                <th style="width: 9%"><a href="<?php echo $baseUrl; ?>setting/categories/<?= $page ?>/ac_name/<?= $order ?>"><i
                            class="fa fa-fw fa-sort"></i>Name</a></th>
                <th style="width: 9%"><a href="<?php echo $baseUrl; ?>setting/categories/<?= $page ?>/ac_code/<?= $order ?>"><i
                            class="fa fa-fw fa-sort"></i>Code</a></th>
                <th>Description</th>
                <th>QTY</th>
                <th style="width: 9%"><a href="<?php echo $baseUrl; ?>setting/categories/<?= $page ?>/wdate/<?= $order ?>"><i
                            class="fa fa-fw fa-sort"></i>R.Date</a></th>

                <th>Status</th>
                <th style="width: 15%">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php if (!empty($results)) : ?>
                <?php foreach ($results as $data) { ?>
                    <tr>
                        <td><input type="checkbox" name="category_id[]" value="<?php echo $data->ac_seq; ?>"><a href="#"><?php echo $data->ac_seq; ?></a></td>
                        <td><?php echo $data->ac_name; ?></td>
                        <td><?php echo $data->ac_code; ?></td>
                        <td><?php echo $data->ac_remark; ?></td>
                        <td><?php echo $data->ac_qty; ?></td>
                        <td><?php echo (isset($data->wdate) && $data->wdate != 0) ? date('Y-m-d', $data->wdate) : ''; ?></td>
                        <td class="text-center" style="width: 5%"><?php echo $this->Common_Model->getStatus($data->status);?></td>
                        <td class="text-left">
                            <a class="btn btn-default"
                               href="<?php echo $baseUrl . "category/view/" . $data->ac_seq; ?>">View</a>
                            <a class="btn btn-primary"
                               href="<?php echo $baseUrl . "category/update/" . $data->ac_seq; ?>">Update</a>
                            <a class="btn btn-danger"
                               href="<?php echo $baseUrl . "category/delete/" . $data->ac_seq; ?>">Delete</a>
                        </td>
                    </tr>
                <?php } ?>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>
</form>
<div class="text-center">
    <p><?php echo $links; ?></p>
</div>
<script type="text/javascript">
    $("#checkAll").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });
</script>