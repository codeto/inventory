<?php
$baseUrl = base_url().'index.php/';
?>
<h1><?php echo $title; ?></h1>
<?php if(validation_errors() != false) { ?>
	<div class="alert alert-danger" role="alert">
		<?php echo validation_errors(); ?>	
	</div>
<?php } ?>
<?php echo form_open('staff/update/'.$employee_item->s_seq) ?>
	<div class="row">
		<div class="col-md-1">
			<div class="form-group <?php echo form_error('s_type') ? 'has-error': ''; ?>">
				<label for="title">Type</label>
				<?php
				echo form_dropdown('s_type', array(0 => 'Admin', 1 => 'Staff'), set_value('s_type'), 'class="form-control"');
				?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<div class="form-group <?php echo form_error('s_department_id') ? 'has-error': ''; ?>">
				<label for="title">Department</label>
				<?php
				echo form_dropdown('s_department_id', $all_department, set_value('s_department_id', $employee_item->s_department_id) , 'class="form-control"');
				?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group <?php echo form_error('s_userid') ? 'has-error': ''; ?>">
				<label for="title">ID</label>
				<input type="text" class="form-control" name="s_userid" id="s_userid" value="<?php echo set_value('s_userid',$employee_item->s_userid);?>" placeholder="ID">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo form_error('s_password') ? 'has-error': ''; ?>">
				<label for="title">Password</label>
				<input type="password" class="form-control" name="s_password" id="s_password" value="<?php echo set_value('s_password'); ?>" placeholder="Password">
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group <?php echo form_error('conf_password') ? 'has-error': ''; ?>">
				<label for="title">Password Confirm</label>
				<input type="password" class="form-control" name="conf_password" id="conf_password" value="<?php echo set_value('conf_password'); ?>" placeholder="Password Confirm">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group <?php echo form_error('s_name') ? 'has-error': ''; ?>">
				<label for="title">Name</label>
				<input type="text" class="form-control" name="s_name" id="s_name" value="<?php echo set_value('s_name',$employee_item->s_name);?>" placeholder="Name">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group <?php echo form_error('as_tel') ? 'has-error': ''; ?>">
				<label for="title">Tel</label>
				<input type="text" class="form-control" name="s_tel" id="s_tel" value="<?php echo set_value('s_tel',$employee_item->s_tel);?>" placeholder="Tel">
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="form-group <?php echo form_error('s_remark') ? 'has-error': ''; ?>">
				<label for="title">Remark</label>
				<textarea name="s_remark" class="form-control" rows="3"><?php echo set_value('s_remark',$employee_item->s_remark);?></textarea>
			</div>
		</div>
	</div>

    <div class="row">
        <div class="col-md-1">
            <div class="form-group <?php echo form_error('status') ? 'has-error': ''; ?>">
                <label for="title">Status</label>
                <?php
                echo form_dropdown('status', array(0 => 'N', 1 => 'Y'), set_value('status', $employee_item->status) , 'class="form-control"');
                ?>
            </div>
        </div>
    </div>
    <input type="submit" class="btn btn-primary" name="submit" value="Update" />
<?php echo form_close(); ?>