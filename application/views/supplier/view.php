<?php
$baseUrl = base_url().'index.php/';
?>
<div class="col-md-10">
    <h1><?php echo $title;?></h1>
    <div class="well">
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <td style="width: 20%">Supplier Name</td>
                    <td><?php echo $supplier_info->su_name;?></td>
                </tr>
                <tr>
                    <td>Supplier Description</td>
                    <td><?php echo $supplier_info->su_description;?></td>
                </tr>
                <tr>
                    <td>Created At</td>
                    <td><?php echo date('Y:m:d h:i:s',$supplier_info->wdate);?></td>
                </tr>
                <tr>
                    <td>Update At</td>
                    <td><?php echo date('Y:m:d h:i:s',$supplier_info->mdate);?></td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td><?php echo $this->Common_Model->getStatus($supplier_info->status);?></td>
                </tr>
            </table>
        </div>
        <div class="row">
            <a class="btn btn-success" href="<?php echo $baseUrl . "supplier/index";?>">Back</a>
        </div>
    </div>
</div>