				    </div>
				</div>
			</div>
		</div>
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!--		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
		<!-- Include all compiled plugins (below), or include individual files as needed -->
<!--		<script src="--><?php //echo base_url(); ?><!--assets/bootstrap/js/bootstrap.min.js"></script>-->
	</body>
					<script type="text/javascript">
						$(window).load(function(){
							$('#modalList').modal('hide');
							$('#modalAssign').modal('hide');
							$('#modalUnassign').modal('hide');
							$('#modalError').modal('hide');
						});

						function add_person(id,  all )
						{
							$('.alert-danger').html(''); // remove error content
							$('.alert-danger').addClass('hide'); // clear error class
							$('.help-block').empty(); // clear error string
							$.ajax({
								url : "<?php echo base_url()."index.php/asset/quickAssigned/";?>" + id,
								type: "POST",
								dataType: "JSON",
								success: function(data)
								{
									console.log(data);

									$('#asset_name').text(data.asset_item.a_name);
									$('#asset_category_code').text(data.asset_item.a_category);
									$('#asset_id').val(data.asset_item.a_seq);
									$('#department_id').html('<option value="">-- Select Department --</option>');
									$('#employee_id').html('<option value="">-- Select Employee --</option>');
									$.each(data.all_department, function(i){
										$('#department_id').append("<option value='"+data.all_department[i].de_seq	+"'>"+data.all_department[i].de_name+"</option>");
									});
									$.each(data.all_staff, function(i){
										$('#employee_id').append("<option value='"+data.all_staff[i].em_seq	+"'>"+data.all_staff[i].em_first_name+data.all_staff[i].em_last_name+"</option>");
									});
                                    if(all != null) {
                                        $('#modalAssign').modal('show'); // show bootstrap modal when complete loaded
                                    } else {
									    $('#modalList').modal('show'); // show bootstrap modal when complete loaded
                                    }
//									$('.modal-title').text('Edit Person'); // Set title to Bootstrap modal title

								},
								error: function (jqXHR, textStatus, errorThrown)
								{
									console.log(jqXHR);
									console.log(textStatus);
									alert('Error get data from ajax');
								}
							});
						}

                        function edit_person(id)
                        {
                            $('.alert-danger').html(''); // remove error content
                            $('.alert-danger').addClass('hide'); // clear error class
//							$('#form')[0].reset(); // reset form on modals
//                            $('.form-group').removeClass('has-error'); // clear error class
//                            $('.help-block').empty(); // clear error string
                            //Ajax Load data from ajax
                            $.ajax({
                                url : "<?php echo base_url()."index.php/asset/quickUnassigned/";?>" + id,
                                type: "POST",
                                dataType: "JSON",
                                success: function(data)
                                {
                                    if(data.status == false) {
//                                        $('#modalError').modal('show'); // show bootstrap modal when complete loaded
                                    } else {
                                        $('#asset_name_unassign').text(data.asset_item.a_name);
                                        $('#asset_category_code_unassign').text(data.asset_item.a_category);
                                        $('#department_unassign').text(data.department);
                                        $('#employee_unassign').text(data.staff.em_first_name+data.staff.em_last_name);
                                        $('#asset_id_unassign').val(data.asset_item.a_seq);

                                        $('#modalUnassign').modal('show'); // show bootstrap modal when complete loaded
                                    }
//									$('.modal-title').text('Edit Person'); // Set title to Bootstrap modal title

                                },
                                error: function (jqXHR, textStatus, errorThrown)
                                {
                                    console.log(jqXHR);
                                    console.log(textStatus);
                                    alert('Error get data from ajax');
                                }
                            });
                        }

						function save()
						{
//							$('#btnSave').attr('disabled',true); //set button disable
                            $('#btnSave').text('saving...'); //change button text
							var url = "<?php echo base_url()."index.php/asset/ajax_add";?>";
							$.ajax({
								url : url,
								type: "post",
								dataType: "text",
                                data: {
                                    employee_id: $("#employee_id").val(),
                                    asset_id :$("#asset_id").val(),
                                    department_id :$("#department_id").val()
                                },
								success: function(data)
								{
                                    if (JSON.parse(data).hasOwnProperty('error') && JSON.parse(data).error > 0){
                                        var html = '';
                                        $.each(JSON.parse(data), function(key, item){
                                            if (key != 'error'){
                                                html += '<p>'+item+'</p>';
                                            }
                                        });
                                        $('.alert-danger').html(html).removeClass('hide');
                                    }
                                    else{
                                        $('#modalList').modal('hide');
                                        location.reload();
                                    }
								},
								error: function (jqXHR, textStatus, errorThrown)
								{
									$('#btnSave').text('save'); //change button text
//									$('#btnSave').attr('disabled',false); //set button enable
								}
							});
						}

                        function assign_new()
                        {
                            $('#btnSave').text('saving...'); //change button text
                            var url = "<?php echo base_url()."index.php/asset/ajax_add";?>";
                            $.ajax({
                                url : url,
                                type: "post",
                                dataType: "text",
                                data: {
                                    employee_id: $("#employee_id").val(),
                                    asset_id :$("#asset_id").val(),
                                    department_id :$("#department_id").val()
                                },
                                success: function(data)
                                {
                                    if (JSON.parse(data).hasOwnProperty('error') && JSON.parse(data).error > 0){
                                        var html = '';
                                        $.each(JSON.parse(data), function(key, item){
                                            if (key != 'error'){
                                                html += '<p>'+item+'</p>';
                                            }
                                        });
                                        $('.alert-danger').html(html).removeClass('hide');
                                    }
                                    else{
                                        $('#modalList').modal('hide');
                                        location.reload();
                                    }
                                },
                                error: function (jqXHR, textStatus, errorThrown)
                                {
                                    $('#btnSave').text('save'); //change button text
//									$('#btnSave').attr('disabled',false); //set button enable
                                }
                            });
                        }

                        function unassigned()
                        {
//							$('#btnSave').attr('disabled',true); //set button disable
                            $('#btnUnassigned').text('saving...'); //change button text
                            var url = "<?php echo base_url()."index.php/asset/ajax_unassigned";?>";
                            $.ajax({
                                url : url,
                                type: "post",
                                dataType: "text",
                                data: {
                                    employee_id: $("#employee_id").val(),
                                    asset_id :$("#asset_id_unassign").val(),
                                    department_id :$("#department_id").val()
                                },
                                success: function(data)
                                {
                                    if(JSON.parse(data).status == true) {
                                        console.log(data);
                                        $('#modalUnassign').modal('hide');
                                        location.reload();
                                    } else {
//                                        var html = '<div class="alert alert-danger">You can not unassigned this asset</div>';
//                                        $(".unassign_modal").html(html);
                                    }
                                },
                                error: function (jqXHR, textStatus, errorThrown)
                                {
                                    $('#btnSave').text('save'); //change button text
//									$('#btnSave').attr('disabled',false); //set button enable
                                }
                            });
                        }
					</script>
</html>