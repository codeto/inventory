<?php
$baseUrl = base_url().'index.php/';
$session_data = $this->session->userdata('logged_in') ? $this->session->userdata('logged_in') : $this->session->userdata('user_logged_in');
//var_dump($session_data);die;
?>
<html>
<head>
<title>Inventory Management System </title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/style.css'); ?>">
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="<?php echo base_url('assets/bower_components/jquery/jquery-1.12.2.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/bower_components/moment/min/moment.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'); ?>"></script>
    <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css'); ?>" />
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
</head>
<body>
<div class="container-fluid">
	<nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url('index.php/employee/index');?>">Hey!Korean IMS </a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <?php if($session_data['name'] == 'session_admin') { ?>
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Assets
						<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo base_url('index.php/asset/index');?>">All Assets</a></li>
						<li><a href="<?php echo base_url('index.php/asset/assigned');?>">Assigned</a></li>
						<li><a href="<?php echo base_url('index.php/asset/unassigned');?>">Un-Assigned</a></li>
					</ul>
				</li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Suppliers
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url('index.php/supplier/index');?>">Suppliers</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Dept
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url('index.php/department/index');?>">Dept</a></li>
                        <li><a href="<?php echo base_url('index.php/employee/index');?>">Employee</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Settings
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
<!--                        <li><a href="--><?php //echo base_url('index.php/admin/info');?><!--">My Info</a></li>-->
                        <li><a href="<?php echo base_url('index.php/setting/categories');?>">Category</a></li>
                        <li><a href="<?php echo base_url('index.php/setting/staffs');?>">Staffs</a></li>
                    </ul>
                </li>

                <?php } ?>

                <?php if($session_data['name'] == 'session_member') { ?>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Settings
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url('index.php/employee/info');?>">My Info</a></li>
                            <li><a href="<?php echo base_url('index.php/asset/member');?>">My Asset</a></li>
                        </ul>
                    </li>
                <?php } ?>
            </ul>
              <ul class="nav navbar-nav navbar-right">
                  <li><a href="#">Hi <?php echo $session_data['username'];?></a></li>
                  <li><a href="<?php echo base_url('index.php/login/logout');?>">Sign Out</a></li>
              </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
	<div class="row">
        <div class="col-sm-12 col-md-12">
            <?php
            echo generateBreadcrumb();
            ?>
