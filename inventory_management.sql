/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : inventory_management

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2016-04-28 17:24:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `ad_seq` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `wdate` int(11) DEFAULT NULL,
  `mdate` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`ad_seq`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'admin', 'e10adc3949ba59abbe56e057f20f883e', null, '121212', '1212121212', null, null);

-- ----------------------------
-- Table structure for assets
-- ----------------------------
DROP TABLE IF EXISTS `assets`;
CREATE TABLE `assets` (
  `a_seq` int(11) NOT NULL AUTO_INCREMENT,
  `a_name` varchar(255) DEFAULT NULL,
  `a_category` int(11) DEFAULT NULL,
  `a_description` varchar(255) DEFAULT NULL,
  `a_supplier_id` int(11) DEFAULT NULL,
  `a_cost` int(11) DEFAULT NULL,
  `a_model` varchar(255) DEFAULT NULL,
  `a_other_detail` varchar(255) DEFAULT NULL,
  `a_purchase_date` int(11) DEFAULT NULL,
  `a_asset_code` varchar(255) DEFAULT NULL,
  `wdate` int(11) DEFAULT NULL,
  `mdate` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `a_is_expendables` char(1) DEFAULT 'N',
  `a_pdate` int(11) DEFAULT NULL,
  PRIMARY KEY (`a_seq`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of assets
-- ----------------------------
INSERT INTO `assets` VALUES ('24', 'Keyboar old ', '2', 'Keyboar old ', '3', '121212', '45000', null, '0', null, '1459415016', '1461745352', 'N', '', null);
INSERT INTO `assets` VALUES ('25', 'Mouse', '1', 'Mouse', '1', '250000', 'MOU3', null, '0', null, '1459417003', '1461835531', 'A', '', null);
INSERT INTO `assets` VALUES ('26', 'Macbook', '6', 'Macbook new', '2', '50000000', 'MAC', null, '0', '', '1459417022', '1461814404', 'N', '', null);
INSERT INTO `assets` VALUES ('27', 'Monitor old', '3', 'Monitor old', '1', '12000000', 'MOU4', null, '1461949200', null, '1459837822', '1461745441', 'N', 'Y', null);
INSERT INTO `assets` VALUES ('28', 'Mouse 2', '1', 'Mouse 2', '1', '12000000', 'MOU2', null, '1459980000', null, '1460359651', '1461835162', 'A', 'Y', null);
INSERT INTO `assets` VALUES ('29', 'Monitor new', '3', 'Monitor new', '5', '200000', 'MONSS', null, '1461189600', null, '1460360174', '1461745169', 'N', '', null);
INSERT INTO `assets` VALUES ('30', 'Macbook', '4', 'Macbook', '2', '800000', 'MAC1', null, '1460239200', null, '1460361542', '1461745464', 'N', '', null);
INSERT INTO `assets` VALUES ('31', 'Monitor update', '3', 'Monitor update', '3', '200000', 'MOU5', null, '1460584800', null, '1461572888', '1461833304', 'A', 'N', null);
INSERT INTO `assets` VALUES ('32', 'Macbook Air', '6', 'Macbook Air', '3', '600000', 'MACAIR', null, '1461362400', null, '1461572932', '1461745510', 'N', 'N', null);
INSERT INTO `assets` VALUES ('33', 'G100S', '2', 'G100S', '2', '120000', 'G100S', null, '1461276000', null, '1461664945', '1461745532', 'N', 'N', null);
INSERT INTO `assets` VALUES ('34', 'Macbook', '3', 'fffd', '2', '0', '2323232323', null, '1460671200', 'ddd', '1461748252', '1461833294', 'A', 'd', null);
INSERT INTO `assets` VALUES ('35', 'Macbook', '3', 'fffd', '2', '0', '2323232323', null, '1460671200', 'ddd', '1461748270', '1461833210', 'N', 'd', null);
INSERT INTO `assets` VALUES ('36', 'Macbook', '3', 'fffd', '2', '0', '2323232323', null, '1460671200', 'ddd', '1461748408', '1461833180', 'A', 'd', null);
INSERT INTO `assets` VALUES ('37', 'vvvv', '4', 'vvvvv', '2', '200000', 'MOU2', null, '1460671200', null, '1461748450', '1461835782', 'N', 'Y', null);
INSERT INTO `assets` VALUES ('38', 'vvvvvvvvvvv', '3', 'vvvvvvvvvvvvvvvvv', '3', '0', 'eee', null, '1461276000', 'vvvvvvvv', '1461751701', '1461836488', 'N', 'v', null);

-- ----------------------------
-- Table structure for asset_assigned
-- ----------------------------
DROP TABLE IF EXISTS `asset_assigned`;
CREATE TABLE `asset_assigned` (
  `aa_seq` int(11) DEFAULT NULL,
  `a_seq` int(11) NOT NULL,
  `ea_seq` int(11) NOT NULL,
  `wdate` int(11) DEFAULT NULL,
  `mdate` int(11) DEFAULT NULL,
  `status` char(1) DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of asset_assigned
-- ----------------------------

-- ----------------------------
-- Table structure for asset_category
-- ----------------------------
DROP TABLE IF EXISTS `asset_category`;
CREATE TABLE `asset_category` (
  `ac_seq` int(11) NOT NULL AUTO_INCREMENT,
  `ac_code` varchar(20) DEFAULT NULL,
  `ac_name` varchar(255) DEFAULT NULL,
  `ac_qty` int(11) DEFAULT '0',
  `ac_remark` text,
  `wdate` int(11) DEFAULT '0',
  `mdate` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ac_seq`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of asset_category
-- ----------------------------
INSERT INTO `asset_category` VALUES ('1', 'MOU', 'Mouse', '0', 'chuot111111', '1459415016', '1461750360', '0');
INSERT INTO `asset_category` VALUES ('2', 'KEY', 'Keyboard', '0', 'Keyboard', '1459415016', '1461750420', '0');
INSERT INTO `asset_category` VALUES ('3', 'Monitor', 'Monitor', '2', 'Monitor', '1459415016', '1461751701', '1');
INSERT INTO `asset_category` VALUES ('4', 'Laptop', 'Laptop', '0', 'Laptop', '1459415016', '1461748919', '0');
INSERT INTO `asset_category` VALUES ('5', 'Air Purifier', 'Air Purifier', '0', 'Air Purifier', '1459415016', '1459415016', '0');
INSERT INTO `asset_category` VALUES ('6', 'Macbook', 'Macbook', '0', 'Macbook', '1459415016', '1461750387', '1');
INSERT INTO `asset_category` VALUES ('7', 'fffff', 'ffff', '0', 'fff', '1459415016', '1461750392', '1');

-- ----------------------------
-- Table structure for asset_inventory
-- ----------------------------
DROP TABLE IF EXISTS `asset_inventory`;
CREATE TABLE `asset_inventory` (
  `ai_seq` int(11) NOT NULL AUTO_INCREMENT,
  `ai_asset_id` int(11) DEFAULT NULL,
  `ai_inventory_date` datetime DEFAULT NULL,
  `ai_number_in_stock` int(11) DEFAULT NULL,
  `ai_number_assigned` int(11) DEFAULT NULL,
  `ai_other_detail` varchar(255) DEFAULT NULL,
  `ai_status` int(11) DEFAULT NULL COMMENT '0: Bad, 1 : Good',
  `wdate` int(11) DEFAULT NULL,
  `mdate` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ai_seq`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of asset_inventory
-- ----------------------------
INSERT INTO `asset_inventory` VALUES ('29', '24', null, '175', null, '2323', '1', '1461745352', '1461826624', '1');
INSERT INTO `asset_inventory` VALUES ('30', '25', null, '3434', null, '', '1', '1461745377', '1461835531', '1');
INSERT INTO `asset_inventory` VALUES ('31', '26', null, '12', null, '', '1', '1461814404', '1461737601', '1');
INSERT INTO `asset_inventory` VALUES ('32', '27', null, '-1', null, '', '1', '1461745441', '1461816713', '1');
INSERT INTO `asset_inventory` VALUES ('33', '28', null, '11', null, '', '1', '1461745132', '1461835162', '1');
INSERT INTO `asset_inventory` VALUES ('34', '29', null, '10', null, '', '1', '1461745169', '1461663809', '1');
INSERT INTO `asset_inventory` VALUES ('35', '30', null, '10', null, '', '1', '1461745464', '1461663804', '1');
INSERT INTO `asset_inventory` VALUES ('36', '31', null, '0', null, '', '1', '1461745489', '1461833304', '1');
INSERT INTO `asset_inventory` VALUES ('37', '32', null, '332', null, '', '1', '1461745510', '1461644007', '1');
INSERT INTO `asset_inventory` VALUES ('38', '33', null, '11', null, '', '1', '1461745532', '1461745185', '1');
INSERT INTO `asset_inventory` VALUES ('39', '0', null, '33', null, '', '1', '1461748408', null, null);
INSERT INTO `asset_inventory` VALUES ('40', '0', null, '0', null, '', '1', '1461748450', null, null);
INSERT INTO `asset_inventory` VALUES ('41', '0', null, '22', null, '', '1', '1461751701', null, null);

-- ----------------------------
-- Table structure for asset_types
-- ----------------------------
DROP TABLE IF EXISTS `asset_types`;
CREATE TABLE `asset_types` (
  `at_seq` int(11) NOT NULL AUTO_INCREMENT,
  `at_name` varchar(255) DEFAULT NULL COMMENT 'Asset code ',
  `at_description` varchar(255) DEFAULT NULL COMMENT 'Asset description',
  `wdate` int(11) DEFAULT NULL,
  `mdate` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`at_seq`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of asset_types
-- ----------------------------
INSERT INTO `asset_types` VALUES ('1', 'Mouse', 'Mouse', '1459323934', '1459329460', '1');
INSERT INTO `asset_types` VALUES ('3', 'Keyboard', 'Keyboard', '1459324494', '1459329451', '1');
INSERT INTO `asset_types` VALUES ('6', 'Laptop', 'Laptop', '1459329474', null, '1');
INSERT INTO `asset_types` VALUES ('7', 'Safe', 'Safe', '1459329483', null, '1');
INSERT INTO `asset_types` VALUES ('8', 'Label Printer', 'Label Printer', '1459329493', null, '1');
INSERT INTO `asset_types` VALUES ('9', 'Air Purifier', 'Air Purifier', '1459329505', null, '1');

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `de_seq` int(11) NOT NULL AUTO_INCREMENT,
  `de_name` varchar(255) DEFAULT NULL,
  `de_description` varchar(255) DEFAULT NULL,
  `wdate` int(11) DEFAULT NULL,
  `mdate` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`de_seq`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES ('1', 'HR Team', 'HR Team', '1459246687', '1461753930', '0');
INSERT INTO `department` VALUES ('2', 'PHP Team ', 'PHP Team ', '1459246699', '1459397237', '1');
INSERT INTO `department` VALUES ('3', 'Designer Department', 'Designer Department', '1459329857', '1459397231', '0');
INSERT INTO `department` VALUES ('4', 'iOS team', 'iOS team', '1459329867', '1459397227', '1');
INSERT INTO `department` VALUES ('5', 'Android team', 'Android team', '1459329873', '1459397224', '1');
INSERT INTO `department` VALUES ('6', 'Tranlater Department', 'Tranlater Department', '1459397219', '1461229417', '1');
INSERT INTO `department` VALUES ('7', 'Manager', 'Manager', '1459397337', '1459415150', '1');

-- ----------------------------
-- Table structure for employees
-- ----------------------------
DROP TABLE IF EXISTS `employees`;
CREATE TABLE `employees` (
  `em_seq` mediumint(9) NOT NULL AUTO_INCREMENT,
  `em_first_name` varchar(255) DEFAULT NULL,
  `em_last_name` varchar(255) DEFAULT NULL,
  `em_department_id` mediumint(9) DEFAULT NULL,
  `em_cell_mobile` varchar(100) DEFAULT NULL,
  `em_email_address` varchar(255) DEFAULT NULL,
  `em_other_detail` text,
  `wdate` varchar(255) DEFAULT NULL,
  `mdate` varchar(255) DEFAULT NULL,
  `status` mediumint(9) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`em_seq`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of employees
-- ----------------------------
INSERT INTO `employees` VALUES ('1', 'Lee', 'Hyunsoon ', '7', '1-622-701-5898', 'facilisi.Sed.neque@cursusvestibulum.com', 'Hyunsoon Lee', '1451575496', '1459397345', '1', '21232f297a57a5a743894a0e4a801fc3', null);
INSERT INTO `employees` VALUES ('2', 'Hai ', 'Hai ', '3', '23232323', 'ligula.eu.enim@ornare.co.uk', 'Hai ', '1478158038', '1459397388', '1', null, null);
INSERT INTO `employees` VALUES ('3', 'Huyen', 'Huyen', '1', '1-686-525-5681', 'aliquet.molestie.tellus@vehicularisusNulla.com', 'HuyenHuyenHuyen', '1489198567', '1459329975', '1', null, null);
INSERT INTO `employees` VALUES ('4', 'Hang', 'Nguyen', '1', '1-272-875-7025', 'est@Etiambibendumfermentum.com', 'HangHangHangHang', '1434820893', '1459405883', '1', null, null);
INSERT INTO `employees` VALUES ('5', 'Linh', 'Nguyen Thi Thuy ', '1', '1-251-913-9150', 'libero.Donec.consectetuer@mattisCras.co.uk', 'sapien, cursus in, hendrerit consectetuer, cursus et, magna. Praesent interdum', '1451399629', '1459329938', '1', null, null);
INSERT INTO `employees` VALUES ('6', 'Tuan Anh', 'Nguyen', '3', '1-302-332-2056', 'et.magnis@feugiatded.net', 'Tuan Anh Nguyen ', '1435493078', '1459751132', '1', null, null);
INSERT INTO `employees` VALUES ('7', 'Son', 'Ha Anh', '2', '1-807-522-4880', 'parturient.montes@a.com', 'nonummy ultricies ornare, elit elit fermentum risus, at fringilla purus', '1484024033', '1459329827', '1', 'admin', 'e10adc3949ba59abbe56e057f20f883e');
INSERT INTO `employees` VALUES ('8', 'Shim', 'Raymond ', '7', '+84936456190', 'Shim@gmail.com', 'Raymond Shim', '1459397377', '1459506443', '1', null, null);
INSERT INTO `employees` VALUES ('9', 'dfdfdfd', 'fdfdf', '2', 'dfd', 'fdfdf@gmail.com', 'dfdfdfdf', '1461222517', null, '1', null, null);
INSERT INTO `employees` VALUES ('10', 'fgf', 'gggggg', '2', 'gggggggggg', 'gggggggggg@gmail.com', 'ggggggggggggg', '1461222532', null, '1', null, null);
INSERT INTO `employees` VALUES ('11', 'ggg', 'gggggggggg', '2', 'gg', 'hason61vggggn@gmail.com', 'ggg', '1461222543', null, '1', null, null);

-- ----------------------------
-- Table structure for employee_asset
-- ----------------------------
DROP TABLE IF EXISTS `employee_asset`;
CREATE TABLE `employee_asset` (
  `ea_seq` int(11) NOT NULL AUTO_INCREMENT,
  `ea_employee_id` int(11) DEFAULT NULL,
  `ea_asset_id` int(11) DEFAULT NULL,
  `ea_date_out` int(11) DEFAULT NULL COMMENT 'date out inventory',
  `ea_date_returned` int(11) DEFAULT NULL COMMENT 'date returned inventory',
  `ea_other_detail` varchar(255) DEFAULT NULL,
  `ea_condition_out` text COMMENT 'status asset : good, new....',
  `ea_condition_returned` text COMMENT 'status asset : good, new....',
  `wdate` int(11) DEFAULT NULL,
  `mdate` int(11) DEFAULT NULL,
  `status` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`ea_seq`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of employee_asset
-- ----------------------------
INSERT INTO `employee_asset` VALUES ('61', '7', '32', '1459980000', '1461276000', null, 'dđ', '', '1461644007', null, 'N');
INSERT INTO `employee_asset` VALUES ('83', '7', '30', null, null, null, null, null, '1461663804', null, 'N');
INSERT INTO `employee_asset` VALUES ('84', '9', '29', null, null, null, null, null, '1461663809', null, 'N');
INSERT INTO `employee_asset` VALUES ('95', '7', '33', null, null, null, null, null, '1461745185', null, '1');
INSERT INTO `employee_asset` VALUES ('96', '7', '27', '1461816713', null, null, null, null, '1461816713', null, '1');
INSERT INTO `employee_asset` VALUES ('97', '8', '24', '1461826624', null, null, null, null, '1461826624', null, '1');
INSERT INTO `employee_asset` VALUES ('100', '7', '36', '1461833180', null, null, null, null, '1461833180', null, '1');
INSERT INTO `employee_asset` VALUES ('102', '9', '34', '1461833294', null, null, null, null, '1461833294', null, '1');
INSERT INTO `employee_asset` VALUES ('103', '7', '31', '1461833304', null, null, null, null, '1461833304', null, '1');
INSERT INTO `employee_asset` VALUES ('104', '7', '28', '1461835162', null, null, null, null, '1461835162', null, '1');
INSERT INTO `employee_asset` VALUES ('106', '7', '25', '1461835500', '0', null, '', '', '1461835531', '1461835573', 'U');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `ro_seq` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------

-- ----------------------------
-- Table structure for staffs
-- ----------------------------
DROP TABLE IF EXISTS `staffs`;
CREATE TABLE `staffs` (
  `s_seq` int(11) NOT NULL AUTO_INCREMENT,
  `s_userid` varchar(255) DEFAULT NULL,
  `s_password` varchar(255) DEFAULT NULL,
  `s_name` varchar(255) DEFAULT NULL,
  `s_department_id` int(11) DEFAULT NULL,
  `s_tel` varchar(100) DEFAULT '0',
  `s_remark` text,
  `wdate` int(11) DEFAULT '0',
  `mdate` int(11) DEFAULT '0',
  `status` char(1) DEFAULT 'Y',
  `s_type` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`s_seq`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of staffs
-- ----------------------------
INSERT INTO `staffs` VALUES ('7', 'hason6dvn33333@gmail.com', '3+da68wiRz8i80UqQiCUwZ7yJIdivVFYlN8DEsbTqpo5dLPId5XeQuRQCPC/KY2Xk5LRBjkt4Q52c/qs5u5A/A==', 'Ha Anh Son', '3', 'ffffffff', 'toi la son', '1461062142', '1461814260', '1', '0');
INSERT INTO `staffs` VALUES ('8', 'sonhaanh1@gmail.com', '33O+IUBy/ZnLxw9p3zkzo+8NWMwmndjBjUKeYbN8GYwUxtT3lp2LsWGKlr0Gs5g7AWnxR5bhK8zupSn+O+VdUw==', 'Son HA 1234', '5', 'Son HA', 'Son HA', '1461652824', '1461826612', '0', '0');
INSERT INTO `staffs` VALUES ('9', 'sonhaanhthyuy@gmail.', 'fgf', 'Thuy Dinh', '6', 'Thuy Dinh', 'Thuy Dinh', '1461652843', '0', '0', '1');
INSERT INTO `staffs` VALUES ('10', 'emai@gmail.com', '1234', 'Son HA', '2', '+84936456190', 'ssss', '1461810346', '0', '0', '0');
INSERT INTO `staffs` VALUES ('11', 'emaiccccc@gmail.com', 'oAU4OH7psiZ4nR/VzEehuc+OBbaMKogI6Nbx71hvk6vr6D8wQs6UH03ze0GZs5EaftLtbJrM2IGz4IRwSuJGHQ==', 'Son HA', '1', '+84936456190', 'cccc', '1461810634', '1461811107', '0', '0');

-- ----------------------------
-- Table structure for supplier
-- ----------------------------
DROP TABLE IF EXISTS `supplier`;
CREATE TABLE `supplier` (
  `su_seq` int(11) NOT NULL AUTO_INCREMENT,
  `su_name` varchar(255) DEFAULT NULL,
  `su_description` text,
  `wdate` varchar(255) DEFAULT NULL,
  `mdate` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`su_seq`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of supplier
-- ----------------------------
INSERT INTO `supplier` VALUES ('1', 'AP 1008 DH', 'AP 1008 DH', '1450100087', '1461729286', '1');
INSERT INTO `supplier` VALUES ('2', 'Thiet bi vat tu ngan hang', 'Thiet bi vat tu ngan hang', '1465186757', '1461729289', '1');
INSERT INTO `supplier` VALUES ('3', 'TranAnh Computer', 'TranAnh Computer', '1469610248', '1459761288', '0');
INSERT INTO `supplier` VALUES ('4', 'Hanoi Computer', 'Hanoi Computer', '1483968717', '1459329586', '0');
INSERT INTO `supplier` VALUES ('5', 'ddd', 'dđ', '1459760298', '1459761280', '0');
INSERT INTO `supplier` VALUES ('7', 'gggg', 'gggggg', '1459844239', null, '0');
